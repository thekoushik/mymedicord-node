exports.alertToArray=(flashes)=>{
    var alerts=[];
    const colors={
        error:'bg-red',
        success:'bg-green',
        info:'alert-info',
        warning:'alert-warning'
    };
    for(var alert_type in flashes){
        var type='alert-info';
        if(colors[alert_type])
            type=colors[alert_type];
        alerts=alerts.concat(flashes[alert_type].map((msg)=>{
            return {type:type,message:msg.replace(/\n/g,'<br>')};
        }));
    }
    return alerts;
};
exports.isRouteLike=(req,uri)=>req.originalUrl.startsWith(uri)