var mongoose = require('mongoose');
var crypto = require('crypto');
exports.view=require('./view');

exports.createId=(str)=>new mongoose.Types.ObjectId(str)

exports.createToken=(dayAhead=1)=>{
    var date=new Date();
    date.setDate(date.getDate()+dayAhead);
    return {
        token: crypto.randomBytes(16).toString('hex'),
        expire_at: date
    };
}
exports.encodeAuthToken=(user,token)=>Buffer.from(user+':'+token, 'ascii').toString('base64')

exports.decodeAuthToken=(token)=>{
    var data=Buffer.from(token, 'base64').toString('ascii').split(':');
    return {user:data[0],token:data[1]};
}
exports.mongooseErrorToString=(err)=>{
    var errorStr=err.name || "Error";
    if(err.errors){
        errorStr+=": ";
        for(var key in err.errors)
            errorStr+=err.errors[key].message+'<br>';
    }
    return errorStr;
}
exports.newTempFileName=()=>{
    var now=new Date();
    return [now.getFullYear(), now.getMonth(), now.getDate(),'-',process.pid,'-',(Math.random() * 0x100000000 + 1).toString(36)].join('');
}