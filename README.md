# MyMedicord
Medical Repository Web App

## Prerequisites

Node 6.9.0 or higher and Mongo 3.2.0 or higher

## Install

```
npm install
```
and
```
npm install -g nodemon
```

## Start Server

```
npm start
```

## Help

### Config

Change ***config.js*** for database connection and email credentials

### Directory Structure

|Directory|Description|
|:----:|----|
|**controllers**|Functions for routing endpoints|
|**middlewares**|Routing middlewares|
|**models**|DB schema definitions|
|**routes**|Router in json format|
|**seeders**|Database seeding|
|**services**|DB operations(used by controllers)|
|**static**|Static contents like css, js, images for client ui|
|**system**|Internal functions used to manage application modules|
|**utils**|Common utility functions used throughout the application|
|**views**|Html files (with [nunjucks](https://mozilla.github.io/nunjucks))|

### Main Repository: [express-starter](https://github.com/thekoushik/express-starter)