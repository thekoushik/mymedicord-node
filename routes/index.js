var app = require('../index');
//var express = require('express');
var middleware = require('../middlewares');
var controllers = require('../controllers');
/*
var apiRouter = express.Router();
apiRouter.get('/users',controllers.api.userList);
apiRouter.get('/user/:id',middleware.hasRole("admin"),controllers.api.user);
apiRouter.post('/register',controllers.api.userCreate);
apiRouter.get('/profile', middleware.shouldLogin, controllers.api.info);
apiRouter.use(controllers.main.errorHandler);

app.use('/api',apiRouter);

var mainRouter = express.Router();
mainRouter.get('/',controllers.main.index);
mainRouter.get('/login',app.securityManager.csrfProtection, controllers.main.loginPage);
mainRouter.post('/login',app.securityManager.csrfProtection, controllers.main.login);
mainRouter.get('/join',controllers.main.registerPage);
mainRouter.get('/logout',controllers.main.logout);
mainRouter.use(controllers.main.errorHandler);
mainRouter.get('*', controllers.main.notFound);

app.use(mainRouter);
*/
const routerJson=[
    {
        path:"/",
        controller: controllers.main.index
    },{
        path:"/login",
        controller: controllers.auth.loginPage,
        middleware: [ app.securityManager.csrfProtection ]
    },{
        path:"/login",
        method:"post",
        controller: controllers.auth.login,
        middleware: [ app.securityManager.csrfProtection ]
    },{
        path: "/join",
        controller: controllers.auth.registerPage
    },{
        path: "/logout",
        controller: controllers.auth.logout
    },{
        path: "/resend_verify",
        controller: controllers.auth.resend_verify_page
    },{
        path: "/resend_verify",
        method:'post',
        controller: controllers.auth.resend_verify
    },{
        path: "/verify",
        controller: controllers.auth.verify
    },{
        path: "/forgot",
        controller: controllers.auth.forgot_page
    },{
        path: "/forgot",
        method:"post",
        controller: controllers.auth.forgot
    },{
        path: "/reset",
        controller: controllers.auth.reset_page
    },{
        path: "/reset",
        method:"post",
        controller: controllers.auth.reset
    },{
        path:"/api",
        children:[
            {
                path:"/setuserposition",
                method:"post",
                controller:controllers.api.setUserPosition
            },{
                path:"/symptoms",
                controller: controllers.api.searchSymptoms
            },{
                path:"/illness",
                controller: controllers.api.searchIllness
            },{
                path:"/users",
                controller: controllers.api.userList
            },{
                path:"/doctors/all",
                controller: controllers.api.doctorList
            },{
                path:"/doctors/new",
                method:"post",
                controller: controllers.api.new_doctor_post
            },{
                path:"/patients/all",
                controller: controllers.api.patientList
            },{
                path:"/patients/new",
                controller: controllers.api.new_patient_post,
                method:"post"
            },{
                path:"/patients/edit/:id",
                method:"post",
                controller: controllers.api.edit_patient_post,
                middleware:[middleware.tempUpload.single('photo')]
            },{
                path:"/patients/reports",
                controller: controllers.api.getHealthReportByActivePatient
            },{
                path: "/patients/select/:id",
                controller: controllers.main.select_patient
            },{
                path: "/patients/healthcases",
                controller: controllers.api.getHealthcasesByPatient
            },{
                path:"/patients/:id",
                controller: controllers.api.get_patient
            },{
                path: "/healthcases/new",
                method: "post",
                controller: controllers.api.createHealthcase
            },{
                path: "/healthcases/edit/:id",
                method: "post",
                controller: controllers.api.edit_healthcase_post
            },{
                path:"/healthcases/search",
                method:"post",
                controller:controllers.api.searchHealtcaseByAny
            },{
                path: "/healthcases/:id",
                controller: controllers.api.viewHealthcase
            },{
                path: "/evolution/new",
                method: "post",
                controller: controllers.api.save_evolution
            },{
                path: "/growth_list",
                controller: controllers.api.growth_list
            },{
                path:"/users/:id",
                controller: controllers.api.user,
                middleware:[ middleware.hasRole("admin") ]
            },{
                path:"/users",
                method:"post",
                controller: controllers.api.userCreate
            },{
                path:"/profile",
                controller: controllers.api.info,
                middleware:[ middleware.shouldLogin ]
            },{
                path: "/profile/basic",
                method: "post",
                controller: controllers.api.saveProfileBasic,
                middleware:[middleware.shouldLogin]
            },{
                path: "/profile/privacy",
                method: "post",
                controller: controllers.api.saveProfilePrivacy,
                middleware:[middleware.shouldLogin]
            },{
                path: "/profile/password",
                method: "post",
                controller: controllers.api.saveProfilePassword,
                middleware:[middleware.shouldLogin]
            },{
                path:"/profile/photo",
                method: "post",
                controller: controllers.api.saveProfilePhoto,
                middleware:[middleware.shouldLogin,middleware.tempUpload.single('file')]
            },{
                path: "/dailyreports",
                controller:controllers.api.getDailyReport
            },{
                path:"/trackreport/:healthcase_id",
                controller:controllers.api.getHealthReport
            },{
                path:"/my_dashboard",
                controller:controllers.api.getDashboard
            },{
                path:"/read_notification/:id",
                controller: controllers.api.readNotification
            }
        ]
    },{
        path: "/admin",
        middleware: middleware.hasRole("admin"),
        children:[
            {
                path: "/",
                controller: controllers.admin.dashboard
            },{
                controller: controllers.main.errorHandler
            }
        ]
    },{
        path: "/feedback",
        children:[
            {
                path:"/thankyou",
                controller: controllers.main.thankyou
            },{
                path:"/:patient_id",
                controller: controllers.main.healthreport_public_page
            },{
                path:"/:patient_id",
                method: "post",
                controller: controllers.main.healthreport_public_post
            }
        ]
    },{
        middleware: [middleware.shouldLogin,middleware.userSession],
        children:[
            {
                path: "/dashboard",
                controller: controllers.main.dashboard
            },{
                path: "/profile",
                controller: controllers.main.profile
            },{
                path: "/profile",
                method: "post",
                controller: controllers.main.save_profile
            },{
                path:"/account_settings",
                controller: controllers.main.account_settings_page
            },{
                path: "/search",
                controller: controllers.main.search_map
            },{
                path: "/doctors",
                children:[
                    {
                        path:'/all',
                        controller: controllers.main.list_doctor_page
                    },{
                        path:'/new',
                        controller: controllers.main.new_doctor_page
                    },{
                        path:'/new',
                        method:"post",
                        controller: controllers.main.new_doctor_post
                    },{
                        path:"/edit/:id",
                        controller: controllers.main.edit_doctor_page
                    },{
                        path: "/edit/:id",
                        method: "post",
                        controller: controllers.main.edit_doctor_post
                    }
                ]
            },{
                path: "/patients",
                children:[
                    {
                        path: "/all",
                        controller: controllers.main.list_patient_page
                    },{
                        path: "/new",
                        controller: controllers.main.new_patient_page
                    },{
                        path: "/new",
                        controller: controllers.main.new_patient_post,
                        method: "post",
                        middleware:[middleware.tempUpload.single('photo')]
                    },{
                        path: "/edit/:id",
                        controller: controllers.main.edit_patient_page
                    },{
                        path: "/edit/:id",
                        method: "post",
                        controller: controllers.main.edit_patient_post
                    }
                ]
            },{
                path: "/healthcases",
                children:[
                    {
                        path: "/all",
                        controller: controllers.main.list_healthcase_page
                    },{
                        path: "/new",
                        controller: controllers.main.create_healthcase_page
                    },{
                        path: "/view/:id",
                        controller: controllers.main.view_healthcase
                    },{
                        path: "/edit/:id",
                        controller: controllers.main.edit_healthcase_page
                    },{
                        path:"/track",
                        controller:controllers.main.track_healthcase_page
                    },{
                        path: "/upload",
                        controller: controllers.main.upload_healthcase_page
                    },{
                        path: "/upload",
                        method: "post",
                        controller: controllers.main.upload_healthcase_post,
                        middleware: [middleware.tempUpload.single('file')]
                    },{
                        path:"/download/:id",
                        controller:controllers.main.downloadHealthcase
                    },{
                        path:"/share",
                        method:"post",
                        controller:controllers.main.shareHealthcaseEmail
                    }
                ]
            },{
                path: "/evolution",
                controller: controllers.main.evolution_page
            },{
                path: "/evolution/birth",
                method: "post",
                controller: controllers.main.evolution_birth_post
            },{
                path: "/evolution/add",
                method: "post",
                controller: controllers.api.save_evolution
            },{
                path: "/reports",
                controller: controllers.main.report_page
            },{
                path: "/uploadfiles",
                method: "post",
                controller: controllers.main.upload_file,
                middleware: [middleware.tempUpload.array('file')/*persistUpload.array('file')*/]
            },{
                path: "/removefile",
                method: "post",
                controller: controllers.main.remove_uploaded_temp_file
            },{
                path: "/growth_summery",
                controller: controllers.main.growth_summery_page
            },{
                path:"/notifications",
                controller: controllers.main.notification_list
            }
        ]
    },{
        controller: controllers.main.errorHandler
    },{
        path: "*",
        controller: controllers.main.notFound
    }
];
exports.router=require('../system').createRouterFromJson(routerJson);