var app=new Vue({
    el: "#report_app",
    data:{
        monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],
        year:null,
        years:[],
        months:[],
        list:[]
    },
    created: function(){
        var self=this;
        $.ajax({
            url:'/api/patients/healthcases',
            dataType: 'json',
            method: 'get',
            success:function(data){
                //console.log(data);
                self.processHealthcases(data);
            },
            error:function(err){
                console.log(err);
            }
        })
        
    },
    mounted:function(){
        var self=this;
        $("#year_select").on('change',function(e){
            var y=$(e.target).val();
            if(y!=""){
                self.year=y
                var mmm=Object.keys(self.list[y]).map(function(mm){return '<option value="'+mm+'">'+self.monthNames[mm]+'</option>'});
                //console.log(mmm);
                $("#month_select").html('<option value="">Select Month</option>'+mmm.join(''))
                $("#month_select").selectpicker('refresh');
            }else{
                $("#month_select").html('<option value="">Select Month</option>');
            }
        })
        $("#month_select").on('change',function(e){
            var m=$(e.target).val();
            if(m=="") return;
            self.showWeekChart('illness_summery',self.list[self.year][m].illness);
            self.showWeekChart('symptom_summery',self.list[self.year][m].symptom);
        })
    },
    methods:{
        showWeekChart:function(id,data){
            var chartdata=[]
            for(var i=0;i<5;i++){
                if(data[i+1])
                    chartdata[i]=data[i+1]
                else
                    chartdata[i]=0
            }
            new Chart(document.getElementById(id).getContext("2d"), {
                type: 'bar',
                data: {
                    labels: ["Week1","Week2","Week3","Week4","Week5"],
                    datasets: [{
                        label: "Weight",
                        data: chartdata,
                        borderColor: 'rgba(0, 188, 212, 0.75)',
                        backgroundColor: 'rgba(0, 188, 212, 0.3)',
                        pointBorderColor: 'rgba(0, 188, 212, 0)',
                        pointBackgroundColor: 'rgba(0, 188, 212, 0.9)',
                        pointBorderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    legend: true
                }
            });
        },
        processHealthcases:function(data){
            const years={};
            data.forEach(function(h){
                var d=moment(h.startDate)
                var dow=Math.ceil(d.date()/7)
                var m=d.month()
                if(years[d.year()]==undefined){
                    var hc={}
                    var illness={}
                    illness[dow]=1
                    var symp={}
                    symp[dow]=h.symptoms.length;
                    hc[m]={
                        illness:illness,
                        symptom:symp
                    }
                    years[d.year()]=hc
                }else if(years[d.year()][m]==undefined){
                    var illness={}
                    illness[dow]=1
                    var symp={}
                    symp[dow]=h.symptoms.length;
                    years[d.year()][m]={
                        illness:illness,
                        symptom:symp
                    }
                }else{
                    if(years[d.year()][m].illness[dow]==undefined)
                        years[d.year()][m].illness[dow]=1
                    else
                        years[d.year()][m].illness[dow]++
                    if(years[d.year()][m].symptom[dow]==undefined)
                        years[d.year()][m].symptom[dow]=h.symptoms.length;
                    else
                        years[d.year()][m].symptom[dow]+=h.symptoms.length;
                }
            })
            this.list=years;
            this.years=Object.keys(this.list);
            //console.log(this.list);
            $("#year_select").html('<option value="">Select Year</option>'+this.years.map(y=>('<option value="'+y+'">'+y+'</option>')).join(''));
            $("#year_select").selectpicker("refresh");
        }
    }
})

