var app=new Vue({
    el: "#evolutionaryinfo",
    data: {
        fileDB:{},
        evo:{
            date:"",
            weight: "",
            height: "",
            identification_mark:"",
            eye_color: "",
            hair_color: "",
            hair_type: ""
        }
    },
    mounted:function(){
        var self=this;
        $('#evo_date').bind('change',function(e,d){
            self.evo.date=d.startOf('day').format('YYYY-MM-DD');
        })
        $(".dropzone").dropzone({
            addRemoveLinks: true,
            withCredentials: true,
            enqueueForUpload: true,
            acceptedFiles: "image/*",
            removedfile: function(file) {
                //console.log(file);
                //var name = file.name; 
                $.ajax({
                    type: 'POST',
                    url: '/removefile',
                    data: {name: self.fileDB[file.upload.uuid].filename},
                    dataType: 'json',
                    success: function(data){
                        //console.log('success: ' + data);
                        delete self.fileDB[file.upload.uuid];
                        console.log(self.fileDB);
                    }
                });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
            success:function(data,file){
                self.fileDB[data.upload.uuid]=file;
                //console.log("Data",data);
                console.log("File",file);
            }
        });
    },
    methods:{
        saveEvolution:function(){
            var self=this;
            var evo=this.evo;
            evo.files=Object.keys(this.fileDB).map(m=>{
                var split=this.fileDB[m].originalname.split(".");
                return {
                    originalname:this.fileDB[m].originalname,
                    filename:this.fileDB[m].filename,
                    ext: split[split.length-1]
                };
            })
            $.ajax({
                type: "post",
                url: '/api/evolution/new',
                dataType: 'json',
                data: evo,
                success:function(data){
                    console.log(data);
                    self.evo={
                        date:"",
                        weight: "",
                        height: "",
                        identification_mark:"",
                        eye_color: "",
                        hair_color: "",
                        hair_type: ""
                    };
                    Dropzone.forElement("#dropzonecomponent").removeAllFiles(true);
                    $("#alert_container").append('<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Evolution detail saved.</div>')
                },
                error:function(err){
                    console.error(err);
                    $("#alert_container").append('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button> Error occured.</div>')
                }
            })
        }
    }
})