var user_detail_app=new Vue({
    el:"#user_detail_app",
    data:{
        user:{
            personal:{info:{}},
            general:{},
            medical:{symptoms:[],medicines:[]}
        }
    },
    methods:{
        setData:function(d){
            this.user.personal=d.personal;
            this.user.general=d.general;
            this.user.medical=d.medical;
        }
    }
})
var map_app=new Vue({
    el:"#map_app",
    data:{
        map:null,
        circle:null,
        markers:[],
        infoWindows:[],
        query:{
            text: "query",
            rad: 0
        },
        list:{},
        map_panel:true,
        map_filters:[
            "symptom",
            "illness",
            "medicine",
            "doctor"
        ],
        cur_filter:0,
        searching:false
    },
    mounted:function(){
        var self=this;
        $("#slider_map").ionRangeSlider({
            min: 0,
            max: 1000,
            from: 0,
            onFinish:function(data){
                self.query.rad=data.from;
            }
        });
    },
    created:function(){
        console.info('Welcome to Global MyMedicord World.');
        this.query.text=document.getElementById('q').value;
    },
    methods:{
        init:function(){
            this.map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: -34.397, lng: 150.644},
                zoom: 8
            });
            var self=this;
            var lt=parseFloat($("#userinfodata").data('userlat'));
            var ln=parseFloat($("#userinfodata").data('userlng'));
            if(isNaN(lt) && isNaN(ln)){
                if(navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var pos = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        self.createMarker(pos,"Me",null,"00FF00")
                        self.map.setCenter(pos);
                    }, function() {
                        self.handleLocationError(true/*, infoWindow, self.map.getCenter()*/);
                    });
                } else {
                    self.handleLocationError(false/*, infoWindow, self.map.getCenter()*/);
                }
            }else{
                var pos={lat:lt,lng:ln};
                self.createMarker(pos,"Me",null,"00FF00")
                self.map.setCenter(pos);
            }
        },
        clearMarkers:function(){
            while(this.markers.length>1){
                var marker=this.markers.pop()
                marker.setMap(null);
            }
        },
        search:function(){
            var self=this;
            //alert(this.query.text+this.query.rad);
            const filter=this.map_filters[this.cur_filter]
            this.searching=true;
            $.ajax({
                url:"/api/healthcases/search",
                method:'post',
                data:{q:this.query.text,filter:filter},
                dataType:'json',
                success:function(data){
                    //console.log('result',data);
                    self.clearMarkers();
                    self.list={};
                    if(data.length==0){
                        swal({title:"No user found",icon:'error'});
                    }else{
                        self.processData(data,filter);
                        self.fitBound();
                    }
                    if(self.query.rad>0)
                        self.createCircle(self.query.rad,self.markers[0].getPosition());
                    self.searching=false;
                },
                catch:function(e){
                    console.error(e);
                    self.searching=false;
                }
            })
        },
        processData:function(data,filter){
            var self=this;
            data.forEach(function(d){
                if(d.user.info){
                    if(d.user.info.lat){
                        var user=d.user;
                        var name=user.settings.privacy.basic?user.name:"Anynomous";
                        var personal=user.settings.privacy.basic?user:{anonymous:true,name:name,email:"Anynomous",info:{phone:"Anynomous",address:"Anynomous"}};
                        var medical={symptoms:[],medicines:[]};
                        var general={};
                        if(filter=="symptom" && user.settings.privacy.symptom)
                            medical.symptoms=[d.name];
                        else if(filter=="doctor" && user.settings.privacy.illness)
                            medical.doctor=d.name;
                        else if(filter=="medicine" && user.settings.privacy.medicine)
                            medical.medicines=[d.name];
                        else if(filter=="illness" && user.settings.privacy.illness)
                            medical.illness=d.illness;
                        if(self.list[user._id]){
                            if(medical.symptoms.length>0)
                                self.list[user._id].medical.symptoms=self.list[user._id].medical.symptoms.concat(medical.symptoms);
                            if(medical.medicines.length>0)
                                self.list[user._id].medical.medicines=self.list[user._id].medical.medicines.concat(medical.medicines);
                        }else{
                            self.list[user._id]={personal:personal,general:general,medical:medical};
                            const id=user._id;
                            self.createMarker({lat:user.info.lat,lng:user.info.lng},name,function(){
                                self.showUserInfo(id);
                            });
                        }
                    }
                }
            })
        },
        handleLocationError:function(browserHasGeolocation, infoWindow, pos) {
            /*infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                                    'Error: The Geolocation service failed.' :
                                    'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(this.map);*/
            swal({
                title: browserHasGeolocation ? 'Error: The Geolocation service failed.' : 'Error: Your browser doesn\'t support geolocation.',
                text: "Map Error",
                icon: "error"
            });
        },
        createMarker:function(pos,title,click,icon){
            var obj={
                position: pos,
                title:title,
                map: this.map
            };
            if(icon){
                obj.icon= new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + icon,
                    new google.maps.Size(21, 34),
                    new google.maps.Point(0,0),
                    new google.maps.Point(10, 34)
                );
            }
            var marker=new google.maps.Marker(obj);
            if(click)
                google.maps.event.addListener(marker, 'click',click);
            this.markers.push(marker);
        },
        createInfoWindow:function(pos,text){
            var infoWindow = new google.maps.InfoWindow;
            infoWindow.setPosition(pos);
            infoWindow.setContent(text);
            infoWindow.open(this.map);
            return infoWindow;
        },
        createCircle:function(rad,pos){
            if(!pos)
                pos=this.map.getCenter()
            if(this.circle)
                this.circle.setMap(null);
            this.circle=new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.3,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.1,
                map: this.map,
                radius: rad * 1000,
                center: pos
            });
        },
        isInRange:function(lat1,lng1,lat2,lng2,rad){
            var R = 6371000;
            var a1 = lat2*(3.14159265/180);
            var a2 = lat1*(3.14159265/180);
            var d1 = (lat1-lat2)*(3.14159265/180);
            var d2 = (lng1-lng2)*(3.14159265/180);
            var a = Math.sin(d1/2) * Math.sin(d1/2) + Math.cos(a1) * Math.cos(a2) * Math.sin(d2/2) * Math.sin(d2/2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c;
            return d/1000>rad;
        },
        fitBound:function(){
            var bounds = new google.maps.LatLngBounds();
            this.markers.forEach(function(m){
                bounds.extend(m.getPosition());
            })
            this.map.fitBounds(bounds);
            /*new google.maps.Rectangle({
                bounds: bounds,
                map: this.map,
                fillColor: "#000000",
                fillOpacity: 0.2,
                strokeWeight: 0
            })*/
        },
        showUserInfo:function(id){
            console.log(this.list[id]);
            user_detail_app.setData(this.list[id]);
            $("#mapModal").modal('show');
            //user_detail_app.$forceUpdate();
        },
        map_panel_off:function(){this.map_panel=false},
        map_panel_on:function(){this.map_panel=true},
        select_filter:function(ind){
            this.cur_filter=ind;
        }
    }
})
