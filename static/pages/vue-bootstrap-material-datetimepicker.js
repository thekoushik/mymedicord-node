Vue.component('k-datetimepicker', {
    //props: ['value','showFormat'],
    props:{
        value: null,
        date: {
            default: function () {
                return moment();
            }
        },
        mainFormat:{
            default: 'dddd DD MMMM YYYY'
        },
        showFormat:{
            default: 'YYYY-MM-DD'
        },
        maxDate: null,
        minDate:null
    },
    template: '<input/>',
    data:function () {
        return {
            val: this.date,
            picker:null
        };
    },
    mounted: function () {
        var vm = this
        var options={
            format: 'dddd DD MMMM YYYY',
            //format:this.dateFormat | 'dddd DD MMMM YYYY',
            clearButton: true,
            weekStart: 1,
            time: false,
            clearButton:false,
        };
        if(this.maxDate){
            options.maxDate=this.maxDate;
        }
        if(this.minDate){
            options.minDate=this.minDate;
        }
        vm.picker=$(this.$el)
        .bootstrapMaterialDatePicker(options)
        .bind('change', function (e,d) {
            if(d){
                vm.$emit('input', d.startOf('day').format(vm.showFormat))
                vm.val=d.startOf('day').format(vm.showFormat);
            }
        })
        if(this.value){
            this.val=this.value;
            vm.picker.val(this.value).trigger('change')
        }
        var name=this.$el.getAttribute('name');
        if(name && window.eventHub)
            window.eventHub.$on(name,function(command){
                if(command=='focus')
                    $(vm.$el).focus();
            })
    },
    watch: {
        value: function (val,oldVal) {
            if(val)
                $(this.$el).val(moment(val).startOf('day').format(this.mainFormat)).trigger('change')
        },
        minDate: function(newVal, oldVal) {
            this.picker.bootstrapMaterialDatePicker('setMinDate',moment(newVal,this.showFormat).format(this.mainFormat))
        },
        maxDate: function(newVal, oldVal) {
            this.picker.bootstrapMaterialDatePicker('setMaxDate',moment(newVal,this.showFormat).format(this.mainFormat))
        }
    },
    destroyed: function () {
        $(this.$el).off().bootstrapMaterialDatePicker('destroy')
    }
})