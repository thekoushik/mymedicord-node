var app = new Vue({
  el: '#app',
  data: {
    currentStep: 0,
    maxStep: 5,
    healthcase_id:"",
    healthcase:{
        name:"",
        startDate:"",
        endDate:"",
        follow_up:"",
        patient:"",
        doctor:"",
        illness:"",
        symptoms:[],
        medicines:[]
    },
    medicine:{
        name:"",
        doses:0,
        unit:0,
        route:0,
        frequency:0,
        direction:0,
        duration:0
    },
    list_db:{
    doses_data:[
        "apply",
        "apply sparingly",
        "0.1",
        "0.25",
        "0.5",
        "1",
        "2",
        "3",
        "4",
        "1-2",
        "2-3",
        "2-4",
        "3-4",
        "5",
        "5-10",
        "6",
        "7",
        "8",
        "9",
        "10",
        "10-15",
        "15",
        "20",
        "10-20",
        "25",
        "30",
        "40",
        "50",
        "60",
        "70",
        "75",
        "80",
        "90",
        "100",
        "125",
        "150",
        "175",
        "200",
        "225",
        "250",
        "275",
        "300",
        "400",
        "500",
        "600",
        "750",
        "875",
        "1000"
        ],
    unit_data:[
        "tab(s)",
        "cap(s)",
        "mg",
        "mg/kg",
        "mg/m2",
        "g",
        "mcg",
        "mcg/kg",
        "mcg/m2",
        "mEq",
        "mEq/kg",
        "mL",
        "Tbsp",
        "tsp",
        "inhalation(s)",
        "puff(s)",
        "neb(s)",
        "spray(s)",
        "gtt(s)",
        "cm",
        "inch(es)",
        "supp(s)",
        "applicatorful",
        "cartridge(s)",
        "cloth(s)",
        "device(s)",
        "kit(s)",
        "L",
        "lozenge(s)",
        "mask(s)",
        "ng",
        "pack(s)",
        "packet(s)",
        "pad",
        "patch(es)",
        "piece(s)",
        "ring(s)",
        "scoop(s)",
        "strip(s)",
        "system(s)",
        "troche(s)",
        "unit(s)",
        "units/kg",
        "units/m2",
        "vial(s)",
        "wafer(s)"
        ],
    route_data:[
        "buccally",
        "chewed",
        "orally",
        "sublingually",
        "submucosally",
        "inhaled",
        "in each nostril",
        "in one nostril",
        "in both eyes",
        "in L eye",
        "in R eye",
        "in both ears",
        "in L ear",
        "in R ear",
        "intravaginally",
        "rectally",
        "topically",
        "subcutaneously",
        "intramuscularly",
        "intravenously",
        "injection",
        "intra-arterially",
        "intra-articularly",
        "intracatheter",
        "intracavernously",
        "intracavitary",
        "intradermally",
        "intralesionally",
        "intrapleurally",
        "intrathecally",
        "intratracheally",
        "intraurethrally",
        "intrauterine",
        "intravesically",
        "intravitreally",
        "epidurally",
        "percutaneously"
        ],
    frequency_data:[
        "once",
        "once a day",
        "every morning",
        "every afternoon",
        "every evening",
        "at every bedtime",
        "2 times a day",
        "3 times a day",
        "1-2 times a day",
        "1-3 times a day",
        "1-4 times a day",
        "4 times a day",
        "before meals &amp; at bedtime",
        "after meals &amp; at bedtime",
        "around the clock",
        "while awake",
        "2-3 times a day",
        "2-4 times a day",
        "3-4 times a day",
        "5 times a day",
        "6 times a day",
        "every 15 minutes",
        "every 30 minutes",
        "every hour",
        "every 1-2 hours",
        "every 2 hours",
        "every 2-3 hours",
        "every 3 hours",
        "every 3-4 hours",
        "every 4 hours",
        "every 4-6 hours",
        "every 6 hours",
        "every 6-8 hours",
        "every 8 hours",
        "every 12 hours",
        "every 24 hours",
        "every other day",
        "every 48 hours",
        "once a week",
        "2 times a week",
        "3 times a week",
        "once a month",
        "use as directed"
        ],
    direction_data:[
        "as needed",
        "before meals",
        "after meals",
        "with meals",
        "before every meal",
        "after every meal",
        "in the morning",
        "in the afternoon",
        "in the evening",
        "at bedtime",
        "x1 week",
        "x2 weeks",
        "x3 weeks",
        "x4 weeks",
        "x6 weeks",
        "x8 weeks",
        "alternate nostrils",
        "swish/spit",
        "swish/swallow"
        ],
    duration_data:[
        "x2 doses",
        "x3 doses",
        "x4 doses",
        "x1 day",
        "x2 days",
        "x3 days",
        "x7 days",
        "x10 days",
        "x1 week",
        "x2 weeks",
        "x3 weeks",
        "x4 weeks",
        "x6 weeks",
        "x8 weeks",
        "x12 weeks",
        "x1 month",
        "x2 months",
        "x3 months",
        "x6 months"
        ]
    },
    doctors:[],
    patients: [],
    hc_files:[],
    old_symptoms:[],
    future:moment()
  },
  mounted:function(){
    var self=this;
    /*$("#start_date").bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY',
        clearButton: true,
        weekStart: 1,
        time: false,
        maxDate:moment()
    }).on('change',function(e,d){
        self.healthcase.startDate=d.startOf('day').format('YYYY-MM-DD');
    });
    $("#end_date").bootstrapMaterialDatePicker({
        format: 'dddd DD MMMM YYYY',
        clearButton: true,
        weekStart: 1,
        time: false,
        maxDate:moment()
    }).on('change',function(e,d){
        self.healthcase.endDate=d.startOf('day').format('YYYY-MM-DD');
    });*/
    $("#symptom_select").on("change",function(e){
        self.healthcase.symptoms=$(e.target).val()==null?[]:$(e.target).val();
    })
    $("#illness").on("change",function(e){
        self.healthcase.illness=$(e.target).val();
    })
  },
  created:function(){
    var self=this;
    this.healthcase_id=$("#healthcase_subheading").data("healthcaseid");
    $.ajax({
        url:'/api/doctors/all',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            if(data.length)
                self.doctors=data;
        },
        catch:function(err){
            console.error(err);
        },
        complete:function(){
            $("#doctors").selectpicker("destroy").select2({
                theme: "bootstrap",
                placeholder: "Select Doctor"
            }).on("change",function(e){
                self.healthcase.doctor=$(e.target).val();
            });
        }
    })
    $.ajax({
        url:'/api/patients/all',
        type: 'get',
        dataType: 'json',
        success: function (data) {
            if(data.length)
                self.patients=data;
        },
        catch:function(err){
            console.error(err);
        },
        complete:function(){
            $("#patients").selectpicker("destroy").select2({
                theme: "bootstrap",
                placeholder: "Select Patient"
            }).on("change",function(e){
                self.healthcase.patient=$(e.target).val();
            });
        }
    });
    $.ajax({
        url: "/api/healthcases/"+this.healthcase_id,
        dataType:"json",
        type: "get",
        success: function(data){
            console.log(data);
            var sd=moment(data.startDate);
            var ed=data.endDate==null?null:moment(data.endDate);
            self.old_symptoms=data.symptoms;
            self.healthcase={
                name: data.name,
                startDate:sd.format("YYYY-MM-DD"),
                endDate:ed==null?null:ed.format("YYYY-MM-DD"),
                follow_up:data.follow_up,
                patient:data.patient._id,
                doctor:data.doctor._id,
                illness:data.illness,
                symptoms:data.symptoms.map(s=>s._id),
                medicines:data.medicines.map((m)=>{
                    return {
                        _id:m._id,
                        user:m.user,
                        name:m.name,
                        doses:m.doses.index,
                        unit:m.unit.index,
                        route:m.route.index,
                        frequency:m.frequency.index,
                        direction:m.direction.index,
                        duration:m.duration.index
                    }
                })
            }
            if(data.files)
                self.hc_files=data.files.map(function(d){
                    d.old=true;
                    return d
                })
            /*
            $("#start_date").val(sd.format("dddd DD MMMM YYYY"));//.trigger("change");
            if(data.endDate!=null)
                $("#end_date").val(ed.format("dddd DD MMMM YYYY"));//.trigger("change");
            */
            $("#doctors").val(data.doctor._id).trigger("change");
            $("#patients").val(data.patient._id).trigger("change");
            $("#illness").append('<option value="' + data.illness + '">' + data.illness + '</option>');
            $("#illness").val(data.illness).trigger("change");
            data.symptoms.forEach((s)=>{
                $("#symptom_select").append('<option value="' + s._id + '">' + s.name + '</option>');
            })
            $("#symptom_select").val(self.healthcase.symptoms).trigger("change");
        },
        catch:function(err){
            console.error(err)
        }
    })
  },
  computed:{
  },
  methods:{
     addFile:function(){
        $("#pres_file").trigger('click');
      },
      removeFile:function(i){
        //console.log(i,this.hc_files[i])
        var self=this;
        $.ajax('/removefile', {
            method: 'post',
            data: {name: this.hc_files[i].file.filename},
            dataType:'json',
            success:function(){
                self.hc_files.splice(i,1)
            },
            error:function(err){
                console.log('err',err)
            }
        });
     },
     select_doses:function(index){
        this.medicine.doses=index;
     },
     select_unit:function(index){
        this.medicine.unit=index;
     },
     select_route:function(index){
        this.medicine.route=index;
     },
     select_frequency:function(index){
        this.medicine.frequency=index;
     },
     select_direction:function(index){
        this.medicine.direction=index;
     },
     select_duration:function(index){
        this.medicine.duration=index;
     },
     process_old_new:function(old_s,new_s){
        var list=new_s.filter(function(i){return !i.startsWith("__") });
        var new_list=[];
        for(var i=0;i<old_s.length;i++)
            if(list.indexOf(old_s[i]._id)<0)
                new_list.push(old_s[i]._id);
        return new_list;
     },
      next:function(){
          this.currentStep++;
      },
      prev:function(){
          this.currentStep--;
      },
      validate:function(){
        var check=false;
        var obj={
            illness:$("#illness").parent(),
            symptoms:$("#symptom_select").parent()
        };
        for(key in obj)
            if(obj[key].hasClass('has-error'))
                obj[key].removeClass('has-error');
        if(this.healthcase.startDate.length==0){
            this.currentStep=0;
            swal("Select healthcase date").then(function(){
                setTimeout(function(){$("#start_date").focus();},100);
            })
        }else if(this.healthcase.doctor.length==0){
            this.currentStep=2;
            swal("Select a doctor");
        }else if(this.healthcase.illness.length==0){
            this.currentStep=3;
            obj.illness.addClass('has-error');
            swal("Mention your illness");
        }else if(this.healthcase.symptoms.length==0){
            this.currentStep=3;
            obj.symptoms.addClass('has-error');
            swal("Select or mention your symptoms");
        }else
            check=true;
        return check;
      },
      finish:function(){
          if(!this.validate()) return;
          var self=this;
          var hc={
            name:this.healthcase.name,
            startDate:this.healthcase.startDate,
            endDate:this.healthcase.endDate,
            follow_up:this.healthcase.follow_up,
            patient:this.healthcase.patient,
            doctor:this.healthcase.doctor,
            illness:this.healthcase.illness,
            symptoms:this.healthcase.symptoms,
          }
          hc.medicines=this.healthcase.medicines.map(function(m){
            var med={
                name:m.name,
                doses:{
                    index:m.doses,
                    text: self.list_db.doses_data[m.doses]
                },
                unit:{
                    index:m.unit,
                    text: self.list_db.unit_data[m.unit]
                },
                route:{
                    index:m.route,
                    text: self.list_db.route_data[m.route]
                },
                frequency:{
                    index:m.frequency,
                    text: self.list_db.frequency_data[m.frequency]
                },
                direction:{
                    index:m.direction,
                    text: self.list_db.direction_data[m.direction]
                },
                duration:{
                    index:m.duration,
                    text: self.list_db.duration_data[m.duration]
                }
            };
            if(m._id){
                med._id=m._id;
                med.user=m.user;
            }
            return med;
        });
        hc.files=this.hc_files;
        var deleted_symptoms=this.process_old_new(this.old_symptoms,hc.symptoms);
        if(deleted_symptoms.length>0)
            hc.deleted_symptoms=deleted_symptoms;
        $.ajax({
            url:'/api/healthcases/edit/'+this.healthcase_id,
            type: 'post',
            data: hc,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if(data.status==1){
                    swal(data.msg).then(function(){
                        window.location.href='/healthcases/view/'+self.healthcase_id;
                    });
                }
            },
            catch:function(err){
                console.error(err);
            },
            complete:function(){
                console.log("End loading");
            }
        })
      },
      add_medicine:function(){
          if(this.medicine.name.length==0) return;
          this.healthcase.medicines.push({
                name:this.medicine.name,
                doses:this.medicine.doses,
                unit:this.medicine.unit,
                route:this.medicine.route,
                frequency:this.medicine.frequency,
                direction:this.medicine.direction,
                duration:this.medicine.duration
          });
          this.medicine={
            name:"",
            doses:0,
            unit:0,
            route:0,
            frequency:0,
            direction:0,
            duration:0
        };
      },
      remove_medicine:function(i){
        this.healthcase.medicines.splice(i,1);
      }
  }
})