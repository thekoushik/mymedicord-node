Vue.use(VeeValidate);
var acc_app=new Vue({
    el: "#acc_app",
    data:{
        user_fetching:true,
        user:{
            info:{
                policy_validity:{}
            }
        },
        user_info_edit_mode:false,
        location_obj:null,
        saving:{
            basic:false,
            privacy:false,
            password:false,
            notification:false
        },
        security:{
            password:"",
            newpassword:"",
            repassword:""
        },
        error:{
            password:false
        },
        patients:[],
        demo:false
    },
    mounted:function(){
        var self=this;
        $("#dob").bind('change',function(e,d){
            self.user.info.dob=d.format('MM/DD/YYYY');
        })
        $("#policy_validity_from").bind('change',function(e,d){
            self.user.info.policy_validity.from=d.format('MM/DD/YYYY')
        })
        $("#policy_validity_to").bind('change',function(e,d){
            self.user.info.policy_validity.to=d.format('MM/DD/YYYY')
        })
        self.location_obj=new google.maps.places.Autocomplete(
            document.getElementById('location'),
            {
                types:['address']
            }
        );
        self.location_obj.addListener('place_changed',function(){
            var place = self.location_obj.getPlace();
            console.log('place',place)
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.

                //this.$emit('no-results-found', place, this.id);
                return;
            }
            self.user.info.lat=place.geometry.location.lat()
            self.user.info.lng=place.geometry.location.lng()
            self.user.info.geo_url=place.url
            if (place.address_components !== undefined){
                place.address_components.forEach(function(c){
                    if(c.types.indexOf('country')>=0)
                        self.user.info.country=c.long_name
                    if(c.types.indexOf('administrative_area_level_1')>=0)
                        self.user.info.state=c.long_name
                    if(c.types.indexOf('administrative_area_level_2')>=0)
                        self.user.info.district=c.long_name
                })
            }
            self.user.info.location=place.formatted_address
        })
    },
    created:function(){
        var self=this;
        $.ajax({
            url:'/api/profile',
            method: 'get',
            dataType: 'json',
            success:function(data){
                self.demo=data.demo_user;
                delete data.demo_user;
                self.init_user(data);
                self.user_fetching=false;
            },
            error:function(err){
                console.log(err);
            }
        })
        $.ajax({
            url:'/api/patients/all',
            method:'get',
            dataType:'json',
            success:function(data){
                self.patients=data
            },
            error:function(err){
                console.log(err);
            }
        })
    },
    filters:{
        date:function(value){
            if (!value) return ''
            return moment(value).format('dddd DD MMMM YYYY');
        }
    },
    methods:{
        init_user:function(data){
            this.user=data;
            if(this.user.info==undefined)
                this.user.info={
                    policy_validity:{}
                }
            else if(this.user.info.policy_validity==undefined)
                this.user.info.policy_validity={}
            
            if(this.user.info.photo)
                $("#account_avatar")[0].src='/gallery/'+this.user.info.photo;
            if(this.user.info.dob)
                $("#dob").val(moment(this.user.info.dob).format("dddd DD MMMM YYYY"));
            if(this.user.info.policy_validity.from)
                $("#policy_validity_from").val(moment(this.user.info.policy_validity.from).format("dddd DD MMMM YYYY"));
            if(this.user.info.policy_validity.to)
                $("#policy_validity_to").val(moment(this.user.info.policy_validity.to).format("dddd DD MMMM YYYY"));
        },
        new_alert:function(type,data){
            if($("#alert_container").children().length>1)
                $("#alert_container .alert:first-child").fadeOut("slow",function(){$(this).remove();});
            $("#alert_container").append('<div class="alert alert-'+type+' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data+'</div>');
        },
        edit_user_info:function(){
            this.user_info_edit_mode=!this.user_info_edit_mode;
        },
        save_user_basic:function(){
            var self=this;
            this.user_info_edit_mode=false
            console.log(this.user);
            this.saving.basic=true;
            $.ajax({
                url:'/api/profile/basic',
                method: 'post',
                data: this.user,
                dataType: 'json',
                success: function(data){
                    console.log('data_basic',data);
                    self.new_alert('success','Basic information has been updated.');
                    self.init_user(data);
                    self.saving.basic=false;
                },
                error:function(err){
                    console.error('error',err);
                    self.new_alert('error','Basic information update failed.');
                    self.saving.basic=false;
                }
            })
        },
        save_privacy:function(){
            var self=this;
            console.log(this.user);
            $.ajax({
                url:'/api/profile/privacy',
                method: 'post',
                data: {settings:{privacy: this.user.settings.privacy}},
                dataType: 'json',
                success: function(data){
                    console.log('data_privacy',data);
                    self.new_alert('success','Privacy information has been updated.');
                    self.init_user(data);
                    self.saving.privacy=false;
                },
                error:function(err){
                    console.error('error',err);
                    self.new_alert('error','Privacy information update failed.');
                    self.saving.privacy=false;
                }
            })
        },
        validate_password:function(){
            this.$validator.validateAll();
            if (!this.errors.any())
                this.save_password()
        },
        save_password:function(){
            var self=this;
            this.saving.password=true;
            self.error.password=false;
            $.ajax({
                url:'/api/profile/password',
                method:'post',
                data:this.security,
                dataType:'json',
                success:function(data){
                    console.log(data)
                    self.saving.password=false;
                    if(data.status==1){
                        self.new_alert('success','Password has been updated.');
                        self.security={
                            password:"",
                            newpassword:"",
                            repassword:""
                        };
                        self.$validator.reset();
                    }else if(data.status==2){
                        self.error.password=true;
                    }else{
                        self.new_alert('error','Password update failed.');
                    }
                },
                error:function(err){
                    console.error(err);
                    this.saving.password=false;
                    self.new_alert('error','Password update failed.');
                }
            })
        },
        save_notification:function(){

        },
        uploadImage:function(){
            var avatar=document.getElementById('account_avatar');
            var initialAvatarURL;
            var canvas;
            canvas = cropper.getCroppedCanvas({
                width: 312,
                height: 208,
            });
            initialAvatarURL = avatar.src;
            avatar.src = canvas.toDataURL();
            canvas.toBlob(function (blob) {
                var formData = new FormData();
                formData.append('file', blob);
                $.ajax('/api/profile/photo', {
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    /*xhr: function () {
                        var xhr = new XMLHttpRequest();
                        xhr.upload.onprogress = function (e) {
                        var percent = '0';
                        var percentage = '0%';
                        if (e.lengthComputable) {
                            percent = Math.round((e.loaded / e.total) * 100);
                            percentage = percent + '%';
                            $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                        }
                        };
                        return xhr;
                    },*/
                    success: function (data) {
                        //$alert.show().addClass('alert-success').text('Upload success');
                        console.log(data);
                    },
                    error: function () {
                        avatar.src = initialAvatarURL;
                        //$alert.show().addClass('alert-warning').text('Upload error');
                    },
                    complete: function () {
                        //$progress.hide();
                    },
                });
            })
        }
    }
})