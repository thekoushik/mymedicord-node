const app = new Vue({
  el: '#app',
  data: {
    currentStep: 0,
    maxStep: 5,
    healthcase:{
        name:"",
        startDate:"",
        endDate:"",
        follow_up:"",
        patient:"",
        doctor:"",
        illness:"",
        symptoms:[],
        medicines:[]
    },
    medicine:{
        name:"",
        doses:0,
        unit:0,
        route:0,
        frequency:0,
        direction:0,
        duration:0
    },
    list_db:{
    doses_data:[
        "apply",
        "apply sparingly",
        "0.1",
        "0.25",
        "0.5",
        "1",
        "2",
        "3",
        "4",
        "1-2",
        "2-3",
        "2-4",
        "3-4",
        "5",
        "5-10",
        "6",
        "7",
        "8",
        "9",
        "10",
        "10-15",
        "15",
        "20",
        "10-20",
        "25",
        "30",
        "40",
        "50",
        "60",
        "70",
        "75",
        "80",
        "90",
        "100",
        "125",
        "150",
        "175",
        "200",
        "225",
        "250",
        "275",
        "300",
        "400",
        "500",
        "600",
        "750",
        "875",
        "1000"
        ],
    unit_data:[
        "tab(s)",
        "cap(s)",
        "mg",
        "mg/kg",
        "mg/m2",
        "g",
        "mcg",
        "mcg/kg",
        "mcg/m2",
        "mEq",
        "mEq/kg",
        "mL",
        "Tbsp",
        "tsp",
        "inhalation(s)",
        "puff(s)",
        "neb(s)",
        "spray(s)",
        "gtt(s)",
        "cm",
        "inch(es)",
        "supp(s)",
        "applicatorful",
        "cartridge(s)",
        "cloth(s)",
        "device(s)",
        "kit(s)",
        "L",
        "lozenge(s)",
        "mask(s)",
        "ng",
        "pack(s)",
        "packet(s)",
        "pad",
        "patch(es)",
        "piece(s)",
        "ring(s)",
        "scoop(s)",
        "strip(s)",
        "system(s)",
        "troche(s)",
        "unit(s)",
        "units/kg",
        "units/m2",
        "vial(s)",
        "wafer(s)"
        ],
    route_data:[
        "buccally",
        "chewed",
        "orally",
        "sublingually",
        "submucosally",
        "inhaled",
        "in each nostril",
        "in one nostril",
        "in both eyes",
        "in L eye",
        "in R eye",
        "in both ears",
        "in L ear",
        "in R ear",
        "intravaginally",
        "rectally",
        "topically",
        "subcutaneously",
        "intramuscularly",
        "intravenously",
        "injection",
        "intra-arterially",
        "intra-articularly",
        "intracatheter",
        "intracavernously",
        "intracavitary",
        "intradermally",
        "intralesionally",
        "intrapleurally",
        "intrathecally",
        "intratracheally",
        "intraurethrally",
        "intrauterine",
        "intravesically",
        "intravitreally",
        "epidurally",
        "percutaneously"
        ],
    frequency_data:[
        "once",
        "once a day",
        "every morning",
        "every afternoon",
        "every evening",
        "at every bedtime",
        "2 times a day",
        "3 times a day",
        "1-2 times a day",
        "1-3 times a day",
        "1-4 times a day",
        "4 times a day",
        "before meals &amp; at bedtime",
        "after meals &amp; at bedtime",
        "around the clock",
        "while awake",
        "2-3 times a day",
        "2-4 times a day",
        "3-4 times a day",
        "5 times a day",
        "6 times a day",
        "every 15 minutes",
        "every 30 minutes",
        "every hour",
        "every 1-2 hours",
        "every 2 hours",
        "every 2-3 hours",
        "every 3 hours",
        "every 3-4 hours",
        "every 4 hours",
        "every 4-6 hours",
        "every 6 hours",
        "every 6-8 hours",
        "every 8 hours",
        "every 12 hours",
        "every 24 hours",
        "every other day",
        "every 48 hours",
        "once a week",
        "2 times a week",
        "3 times a week",
        "once a month",
        "use as directed"
        ],
    direction_data:[
        "as needed",
        "before meals",
        "after meals",
        "with meals",
        "before every meal",
        "after every meal",
        "in the morning",
        "in the afternoon",
        "in the evening",
        "at bedtime",
        "x1 week",
        "x2 weeks",
        "x3 weeks",
        "x4 weeks",
        "x6 weeks",
        "x8 weeks",
        "alternate nostrils",
        "swish/spit",
        "swish/swallow"
        ],
    duration_data:[
        "x2 doses",
        "x3 doses",
        "x4 doses",
        "x1 day",
        "x2 days",
        "x3 days",
        "x7 days",
        "x10 days",
        "x1 week",
        "x2 weeks",
        "x3 weeks",
        "x4 weeks",
        "x6 weeks",
        "x8 weeks",
        "x12 weeks",
        "x1 month",
        "x2 months",
        "x3 months",
        "x6 months"
        ]
    },
    doctors:[],
    patients: [],
    messages:{
        patient:{
            fetching:false
        },
        doctor:{
            fetching:false
        }
    },
    fileDB:{},
    hc_files:[],
    future:moment()
  },
  mounted:function(){
    var self=this;
    $("#symptom_select").on("change",function(e){
        self.healthcase.symptoms=$(e.target).val();
    })
    $("#illness").on("change",function(e){
        self.healthcase.illness=$(e.target).val();
    })
    $("#pres_file").on('change',function(e){
        if(e.target.files.length==0) return;
        var $progress=$('#pres_upload_progress')
        var $progressBar=$progress.find('.progress-bar');
        $progress.show();
        var formData = new FormData();
        formData.append('file', e.target.files[0]);
        $.ajax('/uploadfiles', {
            method: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            xhr: function () {
                var xhr = new XMLHttpRequest();
                xhr.upload.onprogress = function (e) {
                    var percent = '0';
                    var percentage = '0%';
                    if (e.lengthComputable) {
                        percent = Math.round((e.loaded / e.total) * 100);
                        percentage = percent + '%';
                        $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                    }
                };
                return xhr;
            },
            success: function (data) {
                //$alert.show().addClass('alert-success').text('Upload success');
                console.log(data);
                self.hc_files.push({
                    date:"",
                    report_type:"",
                    file:{
                        filename:data.filename,
                        originalname:data.originalname
                    }
                })
            },
            error: function (er) {
                //avatar.src = initialAvatarURL;
                console.log('error',er);
                //$alert.show().addClass('alert-warning').text('Upload error');
            },
            complete: function () {
                $progress.hide();
            },
        });
    })
  },
  created:function(){
    var self=this;
    this.refreshDoctors();
    this.refreshPatients();
    //this.addFile();
  },
  computed:{
    listPatients:function(){
        return this.splitArray(this.patients,3);
    },
    listDoctors:function(){
        return this.splitArray(this.doctors,4);
    }
  },
  methods:{
      addFile:function(){
        $("#pres_file").trigger('click');
      },
      removeFile:function(i){
        console.log(i,this.hc_files[i])
        var self=this;
        $.ajax('/removefile', {
            method: 'post',
            data: {name: this.hc_files[i].file.filename},
            dataType:'json',
            success:function(){
                self.hc_files.splice(i,1)
            },
            error:function(err){
                console.log('err',err)
            }
        });
      },
      splitArray:function(arr,count){
        var newArray = [];
        while (arr.length > 0)
            newArray.push(arr.splice(0, count)); 
        return newArray;
      },
      refreshPatients:function(){
        var self=this;
        this.messages.patient.fetching=true
        $.ajax({
            url:'/api/patients/all',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                if(data.length){
                    self.patients=data;
                    if(self.healthcase.patient==""){
                        data.every(function(d){
                            if(d.relation=="me"){
                                self.healthcase.patient=d._id;
                                return false
                            }
                        });
                    }
                }
                self.messages.patient.fetching=false
            },
            catch:function(err){
                console.error(err);
            }
        })
      },
      refreshDoctors:function(){
        var self=this;
        $.ajax({
            url:'/api/doctors/all',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                if(data.length){
                    self.doctors=data;
                    if(self.healthcase.doctor==""){
                        self.healthcase.doctor=data[0]._id;
                    }
                }
            },
            catch:function(err){
                console.error(err);
            }
        })
      },
    selectPatientCard:function(id){
        this.healthcase.patient=id;
    },
    selectDoctorCard:function(id){
        this.healthcase.doctor=id;
    },
     select_doses:function(index){
        this.medicine.doses=index;
     },
     select_unit:function(index){
        this.medicine.unit=index;
     },
     select_route:function(index){
        this.medicine.route=index;
     },
     select_frequency:function(index){
        this.medicine.frequency=index;
     },
     select_direction:function(index){
        this.medicine.direction=index;
     },
     select_duration:function(index){
        this.medicine.duration=index;
     },
      next:function(){
          this.currentStep++;
      },
      prev:function(){
          this.currentStep--;
      },
      validate:function(){
        var check=false;
        var obj={
            illness:$("#illness").parent(),
            symptoms:$("#symptom_select").parent()
        };
        for(key in obj)
            if(obj[key].hasClass('has-error'))
                obj[key].removeClass('has-error');
        if(this.healthcase.startDate.length==0){
            this.currentStep=0;
            swal("Select healthcase date").then(function(){window.eventHub.$emit('start_date','focus');})
        }else if(this.healthcase.doctor.length==0){
            this.currentStep=2;
            swal("Select a doctor");
        }else if(this.healthcase.illness.length==0){
            this.currentStep=3;
            obj.illness.addClass('has-error');
            swal("Mention your illness");
        }else if(this.healthcase.symptoms.length==0){
            this.currentStep=3;
            obj.symptoms.addClass('has-error');
            swal("Select or mention your symptoms");
        }else
            check=true;
        return check;
      },
      finish:function(){
          if(!this.validate()) return;
          var self=this;
          var hc={
            name:this.healthcase.name,
            startDate:this.healthcase.startDate,
            endDate:this.healthcase.endDate,
            follow_up:this.healthcase.follow_up,
            patient:this.healthcase.patient,
            doctor:this.healthcase.doctor,
            illness:this.healthcase.illness,
            symptoms:this.healthcase.symptoms,
          }
          hc.medicines=this.healthcase.medicines.map(function(m){
              return {
                  name:m.name,
                  doses:{
                      index:m.doses,
                      text: self.list_db.doses_data[m.doses]
                    },
                    unit:{
                        index:m.unit,
                        text: self.list_db.unit_data[m.unit]
                    },
                    route:{
                        index:m.route,
                        text: self.list_db.route_data[m.route]
                    },
                    frequency:{
                        index:m.frequency,
                        text: self.list_db.frequency_data[m.frequency]
                    },
                    direction:{
                        index:m.direction,
                        text: self.list_db.direction_data[m.direction]
                    },
                    duration:{
                        index:m.duration,
                        text: self.list_db.duration_data[m.duration]
                    }
                }
        });
        /*Object.keys(this.fileDB).map(m=>{
            var split=this.fileDB[m].originalname.split(".");
            return {
                originalname:this.fileDB[m].originalname,
                filename:this.fileDB[m].filename,
                ext: split[split.length-1]
            };
        })*/
        //console.log(hc);
        //return;
        hc.files=this.hc_files;
        $.ajax({
            url:'/api/healthcases/new',
            type: 'post',
            data: hc,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if(data.status==1){
                    swal(data.msg).then(function(){
                        window.location.href='/healthcases/view/'+data.id;
                    });
                }
            },
            catch:function(err){
                console.error(err);
                //self.processError(data.err);
            },
            complete:function(){
                console.log("End loading");
            }
        })
      },
      add_medicine:function(){
          if(this.medicine.name.length==0) return;
          this.healthcase.medicines.push({
                name:this.medicine.name,
                doses:this.medicine.doses,
                unit:this.medicine.unit,
                route:this.medicine.route,
                frequency:this.medicine.frequency,
                direction:this.medicine.direction,
                duration:this.medicine.duration
          });
          this.medicine={
            name:"",
            doses:0,
            unit:0,
            route:0,
            frequency:0,
            direction:0,
            duration:0
        };
      },
      remove_medicine:function(i){
        this.healthcase.medicines.splice(i,1);
      },
      processError:function(e){
        if(e.errors){
            var str=[];
            for(key in e.errors)
                str.push(e.errors[key].message);
            this.new_alert('danger',"<p>"+str.join("</p><p>")+"</p>");
        }else
            this.new_alert('danger','Error occured.');
      },
      new_alert:function(type,data){
        if($("#alert_container").children().length>1)
            $("#alert_container .alert:first-child").fadeOut("slow",function(){$(this).remove();});
        $("#alert_container").append('<div class="alert alert-'+type+' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data+'</div>');
      }
  }
})
var patientApp=new Vue({
    el:"#patientApp",
    data:{
        patient:{
            name: "",
            info:{
                gender:"",
            },
            relation: ""
        }
    },
    methods:{
        savePatient:function(){
            //alert(this.patient.name+"-"+this.patient.relation);
            var self=this;
            $.ajax({
                url:'/api/patients/new',
                type: 'post',
                data: this.patient,
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                success: function (data) {
                    console.log(data);
                    if(data.status==1){
                        self.patient.name="";
                        $("#patientAppModal").modal('hide');
                        app.refreshPatients();
                    }
                },
                catch:function(err){
                    console.error(err);
                },
                complete:function(){
                    console.log("End loading");
                }
            })
        }
    }
})
var doctorApp=new Vue({
    el:"#doctorApp",
    data:{
        doctor:{
            name: "",
            speciality: ""
        }
    },
    methods:{
        saveDoctor:function(){
            //alert(this.patient.name+"-"+this.patient.relation);
            var self=this;
            $.ajax({
                url:'/api/doctors/new',
                type: 'post',
                data: this.doctor,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if(data.status==1){
                        self.doctor.name="";
                        $("#doctorAppModal").modal('hide');
                        app.refreshDoctors();
                    }
                },
                catch:function(err){
                    console.error(err);
                },
                complete:function(){
                    console.log("End loading");
                }
            })
        }
    }
})