Vue.use(VeeValidate);
var app = new Vue({
    el: '#app',
    data: {
        doctor:{
            name:"",
            email:"",
            speciality:"",
            phone:"",
            website:""
        }
    },
    methods:{
        validateBeforeSubmit:function(e) {
            var self=this;
            this.$validator.validateAll().then(function(s){
                if(!s) return;
                $.ajax({
                    url:'/doctors/new',
                    type: 'post',
                    dataType: 'json',
                    data: self.doctor,
                    success: function (data) {
                        //console.log(data);
                        if(data.status==1){
                            self.new_alert('success',data.msg);
                            self.doctor={
                                name:"",
                                email:"",
                                speciality:"",
                                phone:"",
                                website:""
                            };
                            self.$validator.reset();
                        }else{
                            self.new_alert('danger',data.msg);//self.processError(data.err);
                        }
                    },
                    catch:function(err){
                        //console.error(err);
                        self.new_alert('danger','Opps, cannot connect to server');
                    }
                })
            });
        },
        processError:function(e){
            if(e.errors){
                var str=[];
                for(key in e.errors)
                    str.push(e.errors[key].message);
                this.new_alert('danger',"<p>"+str.join("</p><p>")+"</p>");
            }else
                this.new_alert('danger','Error occured.');
        },
        new_alert:function(type,data){
            if($("#alert_container").children().length>1)
                $("#alert_container .alert:first-child").fadeOut("slow",function(){$(this).remove();});
            $("#alert_container").append('<div class="alert alert-'+type+' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data+'</div>');
        }
    }
});