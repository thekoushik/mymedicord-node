var app=new Vue({
    el:"#trackapp",
    data:{
        healthcase_db:{},
        pagesize:10,
        lastpage:null,
        report:{
            symptoms:[],
            dates:[],
            fetching:false
        },
        report_db:{}
    },
    created:function(){
        var self=this;
        $.ajax({
            url: '/api/patients/reports',
            method:'get',
            dataType:'json',
            success:function(data){
                console.log('symp',data);
                data.forEach(function(d){
                    if(self.healthcase_db[d.healthcase]==undefined)
                        self.healthcase_db[d.healthcase]=[]
                    self.healthcase_db[d.healthcase].push({report:d._id,symptom:d.symptom} )
                })
                self.fetchReport($("#hc_select").val());
            },
            error:function(err){
                console.log('error',err);
            }
        })
    },
    mounted:function(){
        var self=this;
        $("#hc_select").bind('change',function(e){
            self.fetchReport(e.target.value);
        })
    },
    methods:{
        processData:function(data){
            const smile_text=[
                "Not Sure",
                "Fit and Fine",
                "Fine but Weak",
                "Down",
                "Completely Down",
            ];
            return this.report.dates=data.map(function(d){
                d.date=moment(d.date).format('D MMM, YYYY');
                var rep={};
                d.healthreports.map(function(m){
                    rep[m.healthreport]={
                        value: m.feedback,
                        text: smile_text[m.feedback]
                    };
                })
                d.reports=rep
                return d
            })
        },
        fetchReport:function(hid){
            if(hid){
                var self=this;
                self.report.symptoms=self.healthcase_db[hid];
                if(this.report_db[hid]){
                    this.report.dates=this.report_db[hid]
                }else{
                    self.report.fetching=true;
                    $.ajax({
                        url:'/api/trackreport/'+hid+'?size='+self.pagesize,
                        method:'get',
                        dataType:'json',
                        success:function(data){
                            console.log('rep',data);
                            self.report_db[hid]=self.processData(data);
                            self.report.fetching=false;
                        },
                        error:function(err){
                            console.error(err);
                            self.report.fetching=false;
                        }
                    })
                }
            }
        }
    }
})