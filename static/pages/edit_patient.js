var app = new Vue({
    el: '#app',
    data: {
        patient_id:"",
        patient:{
            name:"",
            email:"",
            relation: "",
            blood: "",
            info:{
                phone: "",
                dob: null,
                gender: "",
                country: "",
                state: "",
                district: "",
                location: "",
                address: "",
                zip_code: "",
                passport: "",
                national_id: "",
                insurance_policy_no: "",
                policy_provider: "",
                policy_validity:{
                    from: null,
                    to: null
                },
                about:[
                    {question:'Hypertension,Chest Pain,lschemic heart disease or any other cardiac disorder.',answer:false,comment:[]},
                    {question:'Tubercolosis,Asthama,Bronchitis or any other lung/respiratory disorder.',answer:false,comment:[]},
                    {question:'Ulcer (Stomach/Duodenal),Hepatities,Cirrhosis or any other digestive or liver /gallbladder disorder.',answer:false,comment:[]},
                    {question:'Renal Failure,Calculus or any other kidney/urinary tract or prostate disorder.',answer:false,comment:[]},
                    {question:'Dizzines,Stroke,Epilepsy,Paralysis or other brain/nervous system disorder.',answer:false,comment:[]},
                    {question:'Diabates,Thyroid Disorder or any other endocrine disorder.',answer:false,comment:[]},
                    {question:'Tumor-benign or malignant,any ulcer/growth/cyst.',answer:false,comment:[]},
                    {question:'Disease of the Nose/Ear/Throat/Teeth /Eye(please mention Dioptres).',answer:false,comment:[]},
                    {question:'HIV /AIDS or sexually transmitted disease or any immune system disorder.',answer:false,comment:[]},
                    {question:'Anaemia,Leukaemia or any other blood / lymphatic system disorder.',answer:false,comment:[]},
                    {question:'Psychiatric / mental illness or sleep disorder.',answer:false,comment:[]},
                    {question:'DUB,Fibroid,Cyst / Fibroadenoma or any other Gynaecological / Breast disorder.',answer:false,comment:[]},
                ],
                emergency_contacts:[
                    {
                        name:"",
                        email:"",
                        phone:"",
                        place:""
                    }
                ]
            }
        },
        photo:null,
        checkLoaded:false,
        blob:null,
        checkQuestions:[
            {question:'Hypertension,Chest Pain,lschemic heart disease or any other cardiac disorder.',answer:false,comment:[]},
            {question:'Tubercolosis,Asthama,Bronchitis or any other lung/respiratory disorder.',answer:false,comment:[]},
            {question:'Ulcer (Stomach/Duodenal),Hepatities,Cirrhosis or any other digestive or liver /gallbladder disorder.',answer:false,comment:[]},
            {question:'Renal Failure,Calculus or any other kidney/urinary tract or prostate disorder.',answer:false,comment:[]},
            {question:'Dizzines,Stroke,Epilepsy,Paralysis or other brain/nervous system disorder.',answer:false,comment:[]},
            {question:'Diabates,Thyroid Disorder or any other endocrine disorder.',answer:false,comment:[]},
            {question:'Tumor-benign or malignant,any ulcer/growth/cyst.',answer:false,comment:[]},
            {question:'Disease of the Nose/Ear/Throat/Teeth /Eye(please mention Dioptres).',answer:false,comment:[]},
            {question:'HIV /AIDS or sexually transmitted disease or any immune system disorder.',answer:false,comment:[]},
            {question:'Anaemia,Leukaemia or any other blood / lymphatic system disorder.',answer:false,comment:[]},
            {question:'Psychiatric / mental illness or sleep disorder.',answer:false,comment:[]},
            {question:'DUB,Fibroid,Cyst / Fibroadenoma or any other Gynaecological / Breast disorder.',answer:false,comment:[]},
        ],
    },
    created:function(){
        var self=this;
        this.patient_id=$("#patient_subheading").data("patientid");
        $.ajax({
            url:'/api/patients/'+this.patient_id,
            method:"get",
            dataType:"json",
            success:function(d){
                console.log(d);
                if(d.info.photo){
                    self.photo=d.info.photo;
                    delete d.info.photo;
                }
                self.patient=d;
                if(d.info.gender){
                    $("#gender_select").val(d.info.gender);
                    $("#gender_select").selectpicker('refresh');
                }
                if(d.relation){
                    $("#relation_select").val(d.relation);
                    $("#relation_select").selectpicker('refresh');
                }
                if(d.blood){
                    $("#blood_select").val(d.blood);
                    $("#blood_select").selectpicker('refresh');
                }
                if(d.info.about.length==0)
                    self.patient.info.about=self.checkQuestions
            },
            error:function(e){
                $("#alert_container").html('<div class="alert alert-danger">Cannot connect to server.</div>');
            }
        })
    },
    mounted:function(){
        var self=this;
        // $('#dob').bind('change',function(e,d){
        //     self.patient.dob=d.format('MM/DD/YYYY');
        // })
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab
            if(target=="#about" && !self.checkLoaded){
                self.checkLoaded=true;
                setTimeout(function(){
                    $('.pat_questions_select').selectpicker("destroy").select2({
                        theme: "bootstrap",
                        placeholder: "Select symptoms",
                        allowClear: true,
                        tags: true,
                    })
                    .on('change',function(e){
                        var i=$(this).data('index');
                        self.checkQuestions[i].comment=$(e.target).val();
                    })
                },100);
            }
        });
        $("#uploadImage").click(function(){
            var avatar=document.getElementById('account_avatar');
            var initialAvatarURL;
            var canvas;
            canvas = window.cropper.getCroppedCanvas({
                width: 312,
                height: 208,
            });
            initialAvatarURL = avatar.src;
            avatar.src = canvas.toDataURL();
            canvas.toBlob(function (blob) {
                self.blob=blob;
                $('#profile_picture_uploader').modal('hide');
            })
        })
    },
    methods:{
        addEmer:function(){
            this.patient.info.emergency_contacts.push({
                name:"",
                email:"",
                phone:"",
                place:""
            })
        },
        removeEmer:function(i){
            this.patient.info.emergency_contacts.splice(i,1);
        },
        savepatient:function(){
            //console.log(this.blob); return;
            var self=this;
            var formData = new FormData();
            formData.append('patient',JSON.stringify(this.patient));
            if(this.blob){
                formData.append('photo', this.blob);
                formData.append('filename',window.filename);
            }
            $.ajax('/api/patients/edit/'+this.patient_id, {
                method: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                credentials: "same-origin",
                /*xhr: function () {
                    var xhr = new XMLHttpRequest();
                    xhr.upload.onprogress = function (e) {
                    var percent = '0';
                    var percentage = '0%';
                    if (e.lengthComputable) {
                        percent = Math.round((e.loaded / e.total) * 100);
                        percentage = percent + '%';
                        $progressBar.width(percentage).attr('aria-valuenow', percent).text(percentage);
                    }
                    };
                    return xhr;
                },*/
                success: function (data) {
                    //$alert.show().addClass('alert-success').text('Upload success');
                    console.log(data);
                    if(data.status==1){
                        $("#alert_container").html('<div class="alert alert-success">'+data.msg+'</div>');
                    }
                },
                error: function () {
                    //avatar.src = initialAvatarURL;
                    //$alert.show().addClass('alert-warning').text('Upload error');
                    $("#alert_container").html('<div class="alert alert-danger">Error occured</div>');
                },
                complete: function () {
                    //$progress.hide();
                },
            });
            /*$.ajax({
                url:'/patients/new',
                type: 'post',
                dataType: 'json',
                data: this.patient,
                //xhrFields: { withCredentials: true },
                //crossDomain: true,
                credentials: "same-origin",
                success: function (data) {
                    //console.log(data);
                    if(data.status==1){
                        self.new_alert('success',data.msg);
                    }else{
                        self.processError(data.err);
                    }
                },
                catch:function(err){
                    //console.error(err);
                    self.new_alert('danger','Opps, cannot connect to server');
                }
            })*/
        },
        processError:function(e){
            if(e.errors){
                var str=[];
                for(key in e.errors)
                    str.push(e.errors[key].message);
                this.new_alert('danger',"<p>"+str.join("</p><p>")+"</p>");
            }else
                this.new_alert('danger','Error occured.');
        },
        new_alert:function(type,data){
            if($("#alert_container").children().length>1)
                $("#alert_container .alert:first-child").fadeOut("slow",function(){$(this).remove();});
            $("#alert_container").append('<div class="alert alert-'+type+' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+data+'</div>');
        }
    }
})