$(function(){
    var lt=parseFloat($("#userinfodata").data('userlat'));
    var ln=parseFloat($("#userinfodata").data('userlng'));
    if(lt==NaN && ln==NaN){
        if (navigator.geolocation) {
            swal({
                title: "Please allow location access to track other users in the map!"
            }).then(function(d){
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    $.ajax({
                        url:"/api/setuserposition",
                        data: pos,
                        method:"post",
                        dataType:"json",
                        success:function(data){
                            if(data.status==1) swal({icon:'success'})
                            else{
                                console.log(data)
                                swal({title:'Cannot set location.',icon:'error'})
                            }
                        },
                        error:function(){
                            console.error(data)
                            swal({title:'Cannot set location.',icon:'error'})
                        }
                    })
                }, function() {
                    swal({
                        title:"Location access denied",
                        icon: "error"
                    })
                });
            });
        } else {
            swal({
                title:"Your browser does not support location service.",
                icon: "warning"
            })
        }
    }
    $.getJSON("/api/my_dashboard").done(function(data){
        $("#dashboard-data-pat").text(data.patients)
        $("#dashboard-data-doc").text(data.doctors)
        $("#dashboard-data-hc").text(data.healthcases)
        $("#dashboard-data-med").text(data.medicines)
        if(data.last5Illnesses.length>0)
            $("#dashboard-data-illness").html(data.last5Illnesses.map(function(s){
                return '<tr><td>'+s.illness+'</td><td><span class="badge">'+(new Date(s.date)).toDateString()+'</span></td></tr>';
            }).join(''));
        if(data.last5Symptoms.length>0)
            $("#dashboard-data-symp").html(data.last5Symptoms.map(function(s){
                return '<tr><td>'+s+'</td></tr>';
            }).join(''));
    })
    .fail(function(e){
        console.log(e);
    })
})