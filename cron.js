var mongoose = require('mongoose');
const config = require('./config');
var nunjucks=require('nunjucks');
nunjucks.configure('views', { autoescape: true });
var mailTransporter = require('node-mailjet').connect(config.email.api_key, config.email.api_secret);
const noMail=false;//no mail for quick testing
const service=require('./services');
var models=require('./models');
var fs=require('fs');
var path=require('path');

var sendEmail = exports.sendEmail = (to, subject, html,from_name=config.name,attach=null, from = config.email.from) => {
    if (noMail) return console.log(html);
    var mailRequest={
        FromEmail: from,
        FromName: from_name,
        Subject: subject,
        'Html-part': html,
        Recipients: [{'Email': to}]
    };
    if(attach)
        mailRequest['Attachments']=attach;
    return mailTransporter.post("send")
        .request(mailRequest);
}
var sendEmailDaily=(to,patients)=>{
    return new Promise((resolve,reject)=>{
        nunjucks.render('email/daily_feedback.html',{base_url:config.url,patients:patients},(err,str)=>{
            if(err) reject(err);
            else resolve(sendEmail(to,'Daily Health Feedback',str))
        })
    })
}
function disconnectIfPossible(){
    if(mongoose.connection.readyState>0 && mongoose.connection.readyState<3)
        mongoose.connection.close();
}
function sendDailyMails(){//send emails from here
    service.healthcase_service.getDailyEmailsForPatients()//.getActiveHealthReportsForAll()
    .then((email)=>{
        //console.log(email);
        var promises=[];
        for(var key in email)
            promises.push(sendEmailDaily(key,email[key]))
        mongoose.connection.close();
        return Promise.all(promises);
    })
    .then(()=>{
        disconnectIfPossible();
    })
    .catch((e)=>{
        disconnectIfPossible();
    });
    /*service.healthcase_service.getPatient(req.params.patient_id)
    .then((pat)=>{
        service.healthcase_service.getActiveHealthReports(req.params.patient_id)
        .then((symps)=>{
            //nunjucks.render('user/healthfeedback_public.html',{patient:pat,symptoms:symps})
            sendEmailDaily('thekoushik.universe@gmail.com','87awd5aw4d654');
        })
        .catch((err)=>{})
    })*/
}
function deleteHealthcaseFiles(user){
    return new Promise((res,rej)=>{
        service.healthcase_service.listHealthcase(user)
        .then((list)=>{
            list.forEach((h)=>{
                h.files.forEach((f)=>{
                    fs.unlinkSync(path.resolve(__dirname,'upload',f.file));
                })
            })
            res(true);
        })
        .catch(e=>res(e))
    });
}
function demoClear(){
    service.user_service.getDemoUser()
    .then((u)=>{
        if(!u) return;
        deleteHealthcaseFiles(u)
        .then(()=>{
            var promises=["doctor","healthcase","medicine","symptom","healthreport","notification"].map((a)=>{
                return models[a].deleteMany({user:u._id}).exec();
            });
            promises.push(service.healthcase_service.deleteAllPatientsNotMe(u._id));
            Promise.all(promises)
            .then(()=>{
                disconnectIfPossible();
            })
            .catch((e)=>{
                disconnectIfPossible();
            })
        })
    })
    .catch((e)=>{
        disconnectIfPossible();
    })
}
mongoose.Promise=global.Promise;
mongoose.connect(config.mongoURI,{ useMongoClient: true})
.then(()=>{
    if(process.argv.indexOf("democlear")>=0) demoClear();
    else sendDailyMails();
})
.catch((e)=>{
    console.log("Cannot connect to database");
})