var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var HealthFeedbackSchema   = new Schema({
    patient: {
        type: Schema.ObjectId,
        ref: 'Patient'
    },
    daily_feedback: {
        type: Number,
        default: 0
    },
    remark: String,
    healthreports:[{
        healthreport:{
            type: Schema.ObjectId,
            ref: 'HealthReport'
        },
        feedback: {
            type: Number,
            default: 0
        }
    }],
    date: {
        type: Date,
        default: Date.now
    }
});
module.exports = mongoose.model('HealthFeedback', HealthFeedbackSchema);