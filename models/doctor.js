var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var DoctorSchema   = new Schema({
    name: {
        type: String,
        required: true
    },
    email: String,
    speciality: String,
    phone: String,
    website: String,
    user: {
        type: Schema.ObjectId,
        required: true,
        ref: 'User'
    },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Doctor', DoctorSchema);