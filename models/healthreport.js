var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var HealthReportSchema   = new Schema({
    patient: {
        type: Schema.ObjectId,
        ref: 'Patient'
    },
    healthcase:{
        type: Schema.ObjectId,
        ref: 'HealthCase'
    },
    user:{
        type: Schema.ObjectId,
        ref: 'User'
    },
    symptom: String,
    _symptom: {
        type:Schema.ObjectId,
        ref: 'Symptom'
    },
    enabled: {
        type: Boolean,
        default: true
    },
    closing_date:Date,
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }
});

module.exports = mongoose.model('HealthReport', HealthReportSchema);