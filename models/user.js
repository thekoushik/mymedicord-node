var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const VERIFY_TOKEN = 'VERIFY_TOKEN';
const PASSWORD_RESET_TOKEN = 'PASSWORD_RESET_TOKEN';

var userSchema = new Schema({
  name:  {type:String,required: true},
  email: {type:String,required: true, unique: true},
  username: {type:String,required: true,unique: true},
  password: {type:String,required: true},
  enabled: { type: Boolean, default: false },
  auth_token:{
    token_type: {
      type: String,
      enum: [VERIFY_TOKEN,PASSWORD_RESET_TOKEN],
      default: VERIFY_TOKEN
    },
    token: String,
    expire_at: Date
  },
  info: {
    photo:String,
    phone: String,
    dob: Date,
    gender: String,
    country: String,
    state: String,
    district: String,
    location: String,
    address: String,
    lat: Number,
    lng: Number,
    geo_url:String,
    zip_code: String,
    emergency_contact: String,
    passport: String,
    national_id: String,
    insurance_policy_no: String,
    policy_provider: String,
    policy_validity:{
      from: Date,
      to: Date
    }
  },
  settings:{
    privacy:{
      basic: {
        type: Boolean,
        default: true
      },
      illness:{
        type: Boolean,
        default: true
      },
      symptom:{
        type: Boolean,
        default: true
      },
      medicine:{
        type: Boolean,
        default: true
      }
    },
    email_notification:[
      {
        patient: {
          type: Schema.ObjectId,
          ref: 'Patient'
        },
        email: String,
        send_individual: Boolean,
        interval: Number
      }
    ]
  },
  account_locked: { type: Boolean, default: false },
  account_expired: { type: Boolean, default: false },
  credential_expired: { type: Boolean, default: false },
  roles: [String],
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
  demo_user: {type: Boolean,default:false}
});

module.exports=mongoose.model('User',userSchema);
module.exports.DTOProps='_id name email username created_at';
module.exports.DTOPropsFull='_id name email username enabled account_locked account_expired credential_expired roles created_at info settings demo_user';
module.exports.DTOPropsAuth='_id name email username enabled account_locked account_expired credential_expired roles created_at auth_token';
module.exports.VERIFY_TOKEN=VERIFY_TOKEN;
module.exports.PASSWORD_RESET_TOKEN=PASSWORD_RESET_TOKEN;