var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var MedicineSchema   = new Schema({
    name: {
        type: String,
        required: true
    },
    doses:{
        index: Number,
        text: String
    },
    unit:{
        index: Number,
        text: String
    },
    route:{
        index: Number,
        text: String
    },
    frequency:{
        index: Number,
        text: String
    },
    direction:{
        index: Number,
        text: String
    },
    duration:{
        index: Number,
        text: String
    },
    user: {
        type: Schema.ObjectId,
        required: true,
        ref: 'User'
    },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Medicine', MedicineSchema);
module.exports.schema=MedicineSchema;