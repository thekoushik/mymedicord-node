var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var MedicineSchema=require('./medicine').schema;
var SymptomSchema=require('./symptom').schema;
var HealthReport = require('./healthreport')

var HealthCaseSchema   = new Schema({
    name: String,
    startDate: {
        type: Date,
        required: true
    },
    endDate: Date,
    user: {
        type: Schema.ObjectId,
        required:true,
        ref: 'User'
    },
    patient: {
        type: Schema.ObjectId,
        required:true,
        ref: 'Patient'
    },
    doctor: {
        type: Schema.ObjectId,
        ref: 'Doctor'
    },
    illness: String,
    symptoms: [ {
        type: Schema.ObjectId,
        required:true,
        ref: 'Symptom'
    } ],
    medicines:[ MedicineSchema],
    files:[{
        date:Date,
        report_type:String,
        file: String
    }],
    closed_symptoms:[Schema.ObjectId],
    deleted: { type: Date },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }
});

HealthCaseSchema.post('save',(doc)=>{
    if(doc.endDate) return;
    doc.populate('symptoms').execPopulate()
    .then((hc)=>{
        HealthReport.find({healthcase:hc._id}).exec()
        .then((hr_array)=>{
            const hr_array_short=hr_array.map(m=>String(m._symptom));
            var new_healthreports=hc.symptoms.map((s)=>{
                return {
                    patient: hc.patient,
                    user: hc.user,
                    healthcase: hc._id,
                    symptom: s.name,
                    _symptom: s._id
                }
            }).filter(m=>hr_array_short.indexOf(String(m._symptom))<0);
            if(new_healthreports.length>0)
                HealthReport.create(new_healthreports)
                .then((arr)=>{
                    console.log('post save success',arr.length)
                })
                .catch((err)=>{
                    console.log('post save error',err)
                })
        })
    })
    .catch((err)=>{
        console.log('post save execpopulate error',err)
    });
})

module.exports = mongoose.model('HealthCase', HealthCaseSchema);