var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var SymptomSchema   = new Schema({
    name: {
        type: String,
        required: true
    },
    user: {
        type: Schema.ObjectId,
        required:true,
        ref: 'User'
    },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Symptom', SymptomSchema);