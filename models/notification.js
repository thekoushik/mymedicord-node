var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var NotificationSchema   = new Schema({
    notification_type: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    url: String,
    user: {
        type: Schema.ObjectId,
        required:true,
        ref: 'User'
    },
    read:{
        type: Boolean,
        default: false
    },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Notification', NotificationSchema);