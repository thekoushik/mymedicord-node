var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var moment       = require('moment');

var PatientSchema   = new Schema({
    name: {
        type: String,
        required: true
    },
    email:String,
    /*phone:String,
    dob: Date,
    gender: String,*/
    relation: String,
    blood: String,
    info:{
        photo:String,
        phone: String,
        dob: Date,
        gender: String,
        country: String,
        state: String,
        district: String,
        location: String,
        address: String,
        lat: Number,
        lng: Number,
        geo_url:String,
        zip_code: String,
        emergency_contact: String,
        passport: String,
        national_id: String,
        insurance_policy_no: String,
        policy_provider: String,
        policy_validity:{
            from: Date,
            to: Date
        },
        about:[
            {
                question:String,
                answer:Boolean,
                comment:[String]
            }
        ],
        emergency_contacts:[
            {
                name:String,
                phone:String,
                email:String,
                place:String
            }
        ]
    },
    user: {
        type: Schema.ObjectId,
        required: true,
        ref: 'User'
    },
    evolution:{
        birth:{
            birth_weight: Number,
            birth_mark: String,
            hair_color: String,
            birth_height: Number,
            eye_color: String,
            hair_type: String
        },
        growth:[
            {
                date: Date,
                weight: Number,
                height: Number,
                identification_mark: String,
                eye_color: String,
                hair_color: String,
                hair_type: String,
                files:[ String ]
            }
        ]
    },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }
});
/*
PatientSchema.pre('save',function(next){
    console.log('[pre save]',this.info.dob);
    var newdate=moment(this.info.dob).add({days:1}).format()
    next();
})*/

module.exports = mongoose.model('Patient', PatientSchema);