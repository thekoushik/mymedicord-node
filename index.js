var express = require('express');
var app = module.exports = express();
var moment = require('moment');

app.use('/assets',express.static('static'));
app.use("/gallery", express.static(__dirname + "/upload"));

var nunjucksEnv = require('nunjucks').configure('views', {
    autoescape: true,
    express: app
});
nunjucksEnv.addFilter('date',function(date){
    return moment(date).format('D MMM, YYYY');
});
nunjucksEnv.addFilter('datetime',function(date){
    return moment(date).format('D MMM, YYYY h:mm a');
});
nunjucksEnv.addFilter('ago',function(time){
    return moment.duration(-moment().diff(new Date(time))).humanize(true);
});

module.exports.securityManager = require('./boot');

require('./routes');

var port=process.env.PORT || 80;
app.listen(port,()=>{
    console.log("Listening on 'http://127.0.0.1:"+port);
});
