var Symptom = require('../models').symptom;
var Doctor=require('../models').doctor;
var Healthcase=require('../models').healthcase;
var Patient = require('../models').patient;
var User = require('../models').user;
var XLSX = require('xlsx');
var fs=require('fs');
var moment=require('moment');
var util = require("util");
const medicine_instruction=require('./medicine_instruction');
var HealthReport=require('../models').healthreport;
var HealthFeedback=require('../models').health_feedback;
var Medicine=require('../models').medicine;

exports.findSymptoms=(name,user)=>{
    var q={name: new RegExp('^'+name, 'i')};
    if(user)
        q.user=user._id;
    return Symptom.find(q).exec();
}
exports.meAsPatient=(user)=>{
    return Patient.findOne({
        user: user._id,
        relation: "me"
    }).exec();
}
exports.findIllness=(illness,user)=>{
    var q={illness: new RegExp('^'+illness, 'i')};
    if(user)
        q.user=user._id;
    return Healthcase.find(q).exec();
}
exports.createSymptoms=(symp)=>{
    return Symptom.create(symp);
}
exports.listDoctors=(user_id)=>{
    return Doctor.find({user:user_id}).exec();
}
var searchDoctors=exports.searchDoctors=(name)=>{
    return Doctor.find({name:new RegExp('^'+name,'i')}).exec();
}
exports.createDoctor=doctor=>Doctor.create(doctor)
exports.createPatient=patient=>Patient.create(patient)
var listPatient=exports.listPatient=(user_id)=>Patient.find({user:user_id}).exec()
var createHealthCase=exports.createHealthCase=(data)=>{
    return Healthcase.create(data)
}
exports.listHealthcase=(user)=>{
    return Healthcase.find({user: user._id}).exec();
}
exports.listHealthcaseSortByDate=(user,patient)=>{
    return Healthcase.find({user:user._id,patient:patient._id}).sort('-startDate').exec();
}
exports.listHealthcaseByPatient=(patient_id)=>{
    return Healthcase.find({patient: patient_id}).populate('symptoms').exec();
}
var findHealthcase=exports.findHealthcase=(healthcase_id,user_id)=>{
    return Healthcase.findOne({_id:healthcase_id,user:user_id}).exec();
}
exports.viewHealthcase=(healthcase_id)=>{
    return Healthcase.findById(healthcase_id)
            .populate('patient')
            .populate('doctor')
            .populate('symptoms')
            .populate('medicines')
            .exec();
}
exports.findPatient=(patient_id,user_id)=>{
    return Patient.findOne({_id:patient_id,user: user_id}).exec();
}
var getPatient=exports.getPatient=(patient_id)=>{
    return Patient.findById(patient_id).exec();
}
exports.savePatient=(patient,data)=>{
    for(var key in data)
        patient[key]=data[key];
    return patient.save();
}
exports.findDoctor=(doctor_id,user_id)=>{
    return Doctor.findOne({_id:doctor_id,user:user_id}).exec();
}
exports.saveDoctor=(doctor,data)=>{
    for(var key in data)
        doctor[key]=data[key]
    return doctor.save()
}
exports.removeHealthReportsAndFeedbacksBySymptoms=(symps)=>{
    return new Promise((res,rej)=>{
        if(!symps){
            res(null);
            return;
        }
        HealthReport.find({_symptom:{'$in':symps}}).exec()
        .then((hr)=>{
            const healthReportIds=hr.map(h=>String(h._id));
            HealthFeedback.find({'healthreports.healthreport':{'$in':healthReportIds}}).exec()
            .then((hf)=>{
                console.log(hf);
                var promises=[];
                for(var i=0;i<hf.length;i++){
                    var hfr=hf[i].healthreports;
                    var remove=[];
                    for(var j=0;j<hfr.length;j++)
                        if(healthReportIds.indexOf(String(hfr[j].healthreport))>=0)
                            remove.push(j);
                    console.log('remove',remove);
                    for(var j=0;j<remove.length;j++)
                        hf[i].healthreports.splice(remove[j]);
                    promises.push(hf[i].save());
                }
                Promise.all(promises)
                .then((_)=>{
                    return HealthReport.remove({_id:{'$in':healthReportIds}}).exec();
                })
                .then((_)=>{
                    res(healthReportIds);
                })
                .catch((e)=>{
                    rej(e);
                })
            })
            .catch((e)=>{
                rej(e);
            })
        })
        .catch((e)=>{
            rej(e);
        })
    })
}
exports.saveHealtcase=(hc,data)=>{
    if(data.deleted_symptoms){
        data.deleted_symptoms.forEach((d)=>{
            var i=hc.closed_symptoms.map(m=>String(m)).indexOf(d);
            if(i>=0)
                hc.closed_symptoms.splice(i,1);
        })
        delete data.deleted_symptoms;
    }
    for(var key in data)
        hc[key]=data[key]
    return hc.save();
}
exports.readExcel=(path)=>{
    return new Promise((res,rej)=>{
        fs.readFile(path, (err, data) => {
            if (err) rej(err);
            var workbook = XLSX.read(data, {type:'buffer'});
            fs.unlink(path);
            res(XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]));
        });
    })
}
var getProcessedSymptomByName=(name,user_id)=>{
    return new Promise((res,rej)=>{
        Symptom.findOne({
            name: new RegExp('^'+name,'i'),
            user: user_id
        }).exec()
        .then((symp)=>{
            if(symp)
                res(symp);
            else{
                return Symptom.create({
                    name: name,
                    user: user_id
                })
            }
        })
        .then((symp)=>{
            res(symp);
        })
        .catch((err)=>{
            rej(err);
        })
    })
}
var processSymptomNames=(str,user_id)=>{
    return new Promise((res,rej)=>{
        Promise.all(str.split(',').map(s=>getProcessedSymptomByName(s.trim(),user_id)))
        .then((arr)=>{
            res({symptoms: arr.map(i=>i._id)})
        })
        .catch((err)=>{
            rej(err);
        })
    })
}
var createMedicineFromJSON=(obj)=>{
    var med={
        name: obj['Drug name']
    };
    var any=0;
    [
        {
            name: 'Dose',
            ins: 'doses'
        },{
            name: 'Unit',
            ins: 'unit'
        },{
            name: 'Route',
            ins: 'route'
        },{
            name: 'Frequency',
            ins: 'frequency'
        },{
            name: 'Directions',
            ins: 'direction'
        },{
            name: 'Duration',
            ins: 'duration'
        }
    ].forEach((code)=>{
        if(obj[code.name]!=undefined && obj[code.name]!='N/A'){
            //console.log(code);
            var d=medicine_instruction[code.ins+'_data'].indexOf(obj[code.name].trim())
            if(d>=0){
                med[code.ins]={
                    index: d,
                    text: medicine_instruction[code.ins+'_data'][d]
                };
                any++;
            }
        }
    })
    if(any==0 && med.name==undefined)
        return null;
    return med;
}
var isHealthcaseToBeUpdated=(json)=>{
    return (json['Health Case Name '] &&
            !json['Date'] &&
            !json['Doctor Name'] &&
            !json['Diagnosed with'] &&
            !json['Symptom'] &&
            json['Drug Name'])
}
var findHealthcaseByName=(name,user)=>{
    return Healthcase.findOne({
        user: user,
        name: name
    }).exec();
}
/*var createHealthcaseWithOptionalData=(hc,user,patient)=>{
    return new Promise(function(resG,rejG){
        const healthcase={
            user: user._id,
            name: hc['Health Case Name '],
            startDate: (hc['Date']==undefined?moment():moment(hc['Date'],'DD/MM/YYYY')).format('MM/DD/YYYY')
        };
        if(hc['Diagnosed with'])
            healthcase.illness=hc['Diagnosed with'];
        var promises=[];
        if(hc['Symptom'])
            promises.push(processSymptomNames(hc['Symptom'],user._id))
        var doctor=hc['Doctor name'];
        if(doctor){
            promises.push(new Promise(function(res,rej){
                searchDoctors(doctor)
                .then((docs)=>{
                    if(docs.length)
                        res({doctor: docs[0]._id});
                    else{
                        Doctor.create({
                            name: doctor,
                            user: user._id
                        })
                        .then((doc)=>{
                            res({doctor:doc._id});
                        })
                        .catch((err)=>{
                            rej(err);
                        })
                    }
                })
                .catch((err)=>{
                    rej(err)
                })
            }))
        }
        if(patient)
            healthcase.patient=patient._id
        else{
            promises.push(new Promise((res,rej)=>{
                listPatient(user._id)
                .then((users)=>{
                    res({patient:users[0]._id});
                })
                .catch((err)=>{
                    rej(err);
                })
            }))
        }
        var medicine=createMedicineFromJSON(hc);
        if(medicine!=null){
            healthcase.medicines.push(medicine);
        }
        Promise.all(promises)
        .then((arr)=>{
            for(var i=0;i<arr.length;i++){
                for(var key in arr[i])
                    healthcase[key]=arr[i][key];
            }
            //console.log(util.inspect(healthcase));
            return Healthcase.create(healthcase);
        })
        .then((h)=>{
            resG(h._id);
        })
        .catch((err)=>{
            rejG(err);
        })
    });
}*/
var processJSONHealthcases=(json)=>{
    var healthcases={};
    json.forEach((hc)=>{
        if(healthcases[hc['Health Case Name ']]==undefined){
            healthcases[hc['Health Case Name ']]={
                name: hc['Health Case Name '],
                startDate: (hc['Date']==undefined?moment():moment(hc['Date'],'DD/MM/YYYY')).startOf('day').format('YYYY-MM-DD')
            };
            if(hc['Diagnosed with'])
                healthcases[hc['Health Case Name ']].illness=hc['Diagnosed with'];
            if(hc['Doctor name'])
                healthcases[hc['Health Case Name ']].doctor=hc['Doctor name'];
            if(hc['Symptom'])
                healthcases[hc['Health Case Name ']].symptoms=hc['Symptom'];
            if(hc['Drug name'])
                healthcases[hc['Health Case Name ']].medicines=[
                    createMedicineFromJSON(hc)
                ];
        }else{
            var med=createMedicineFromJSON(hc);
            if(med!=null)
                healthcases[hc['Health Case Name ']].medicines.push(med);
        }
    })
    return Object.keys(healthcases).map(k=>healthcases[k]);
}
exports.jsonToHealthcase=(json_data,user,patient)=>{
    var json=processJSONHealthcases(json_data)
    return Promise.all(
        json.map((healthcase)=>{
            return new Promise(function(resG,rejG){
                const hc=healthcase;
                hc.user=user;
                var promises=[];
                if(hc.symptoms)
                    promises.push(processSymptomNames(hc.symptoms,user._id))
                if(hc.doctor){
                    promises.push(new Promise(function(res,rej){
                        searchDoctors(hc.doctor)
                        .then((docs)=>{
                            if(docs.length)
                                res({doctor: docs[0]._id});
                            else{
                                Doctor.create({
                                    name: hc.doctor,
                                    user: user._id
                                })
                                .then((doc)=>{
                                    res({doctor:doc._id});
                                })
                                .catch((err)=>{
                                    rej(err);
                                })
                            }
                        })
                        .catch((err)=>{
                            rej(err)
                        })
                    }))
                }
                if(patient)
                    hc.patient=patient._id
                else{
                    promises.push(new Promise((res,rej)=>{
                        listPatient(user._id)
                        .then((patients)=>{
                            res({patient:patients[0]._id});
                        })
                        .catch((err)=>{
                            rej(err);
                        })
                    }))
                }
                if(hc.medicines){
                    hc.medicines=hc.medicines.map(m=>{
                        m.user=user;
                        return m
                    })
                }
                Promise.all(promises)
                .then((arr)=>{
                    for(var i=0;i<arr.length;i++){
                        for(var key in arr[i])
                            hc[key]=arr[i][key];
                    }
                    //console.log(util.inspect(healthcase));
                    return Healthcase.create(hc);
                })
                .then((h)=>{
                    resG(h._id);
                })
                .catch((err)=>{
                    rejG(err);
                })
            })
        })
    )
}
exports.saveEvolutionBirth=(data,patient_id)=>{
    return Patient.findById(patient_id).exec()
    .then((pat)=>{
        pat.evolution.birth.birth_weight=data.weight;
        pat.evolution.birth.birth_mark=data.mark;
        pat.evolution.birth.hair_color=data.hair_color;
        pat.evolution.birth.birth_height=data.height;
        pat.evolution.birth.eye_color=data.eye_color;
        pat.evolution.birth.hair_type=data.hair_type;
        return pat.save();
    })
}
exports.saveEvolutionByDate=(data,patient_id)=>{
    return Patient.findById(patient_id).exec()
    .then((pat)=>{
        var files=[]
        if(data.files)
            files=data.files.map((f)=>{
                var name=patient_id+'_'+f.filename+'.'+f.ext;
                fs.rename('tmp/'+f.filename,'upload/'+name);
                return name;
            })
        pat.evolution.growth.push({
            date: data.date,
            weight: data.weight,
            height: data.height,
            identification_mark: data.identification_mark,
            eye_color: data.eye_color,
            hair_color: data.hair_color,
            hair_type: data.hair_type,
            files: files
        })
        return pat.save();
    })
}
exports.getEvolutionGrowth=(patient_id)=>{
    return new Promise((res,rej)=>{
        getPatient(patient_id)
        .then((pat)=>{
            res(pat.evolution.growth.sort((a,b)=>new Date(a.date)-new Date(b.date)))
        })
        .catch((err)=>{
            rej(err)
        })
    }) 
}
/*
Fit and Fine
Fine but Weak
Not Sure
Down
Completely Down
=====================
Close Symptom
Fine but Weak
Not Sure
Down
Completely Down
*/
exports.getActiveHealthReports=(patient_id)=>{
    return new Promise((res,rej)=>{
        HealthReport.find({patient:patient_id,enabled: true}).exec()
        .then((hrs)=>{
            var symptoms={}
            hrs.forEach(function(h){
                if(symptoms[h.symptom])
                    symptoms[h.symptom].healthreports.push(h._id)
                else
                    symptoms[h.symptom]={
                        healthreports:[h._id],
                        symptom: h.symptom
                    };
            })
            res(Object.keys(symptoms).map(m=>symptoms[m]));
        })
        .catch((err)=>{
            rej(err);
        })
    })
}
exports.checkTodaysFeedback=(patient_id)=>{
    var today=new Date();today.setHours(0,0,0,0);
    var tommorow=new Date();tommorow.setHours(23,59,59,999);
    return HealthFeedback.findOne({patient:patient_id, date:{'$gte':today,'$lt':tommorow}}).exec();
}
exports.saveFeedback=(data,patient_id)=>{
    const daily=data.daily; delete data.daily;
    const remark=data.remark; delete data.remark;
    var reports=[];
    var promises=[];
    for(var key in data){
        key.split('.').forEach((f)=>{
            if(data[key]==1)
                promises.push(HealthReport.findByIdAndUpdate(f,{enabled:false, closing_date:Date.now()})/*.populate('healthcase')*/.exec());
            reports.push({
                healthreport:f,
                feedback:data[key]
            })
        })
    }
    return Promise.all(promises)
    .then((hrs)=>{//closing reports
        /*var h_promises=hrs.map((hr)=>{
            //if(!hr.healthcase.closed_symptoms) hr.healthcase.closed_symptoms=[];
            hr.healthcase.closed_symptoms.push(hr._symptom);
            return hr.healthcase.save();
        })
        return Promise.all(h_promises);*/
        var healthcase_symptom={};
        hrs.forEach((h)=>{
            if(!healthcase_symptom[h.healthcase])
                healthcase_symptom[h.healthcase]=[];
            healthcase_symptom[h.healthcase].push(h._symptom);
        })
        var h_promises=[];
        for(var h in healthcase_symptom)
            h_promises.push(Healthcase.findByIdAndUpdate(h,{ '$pushAll': { closed_symptoms: healthcase_symptom[h]}}).exec());
        return Promise.all(h_promises);
    })
    .then((healthcases)=>{
        return Promise.all( healthcases.map(m=>Healthcase.findById(m._id)));
    })
    .then((healthcases)=>{
        var u_promises=[];
        healthcases.forEach((healthcase)=>{
            console.log('Healtcase:',healthcase);
            //console.log(h.symptoms.length,h.closed_symptoms.length);
            if(healthcase.symptoms.length==healthcase.closed_symptoms.length){
                healthcase.endDate=Date.now();
                u_promises.push(healthcase.save());
            }
        })
        return Promise.all(u_promises);
    })
    .then((_)=>{
        return HealthFeedback.create({
            patient:patient_id,
            daily_feedback:daily,
            remark:remark,
            healthreports:reports
        })
    })
}
exports.updateEmptyFeedback=(feedback_id,data,patient_id)=>{
    var daily=data.daily; delete data.daily;
    var remark=data.remark; delete data.remark;
    var reports=[];
    var promises=[];
    for(var key in data){
        key.split('.').forEach((f)=>{
            if(data[key]==1)
                promises.push(HealthReport.findByIdAndUpdate(f,{enabled:false, closing_date:Date.now()}).exec());
            reports.push({
                healthreport:f,
                feedback:data[key]
            })
        })
    }
    promises.push(HealthFeedback.findByIdAndUpdate(feedback_id,{
        daily_feedback:daily,
        remark:remark,
        healthreports:reports
    }).exec());
    return Promise.all(promises);
}
exports.getHealthReportByPatient=(patient_id)=>{
    return HealthReport.find({patient:patient_id}).exec();
}
exports.getHealthFeedbacksByHealthcase=(healthcase_id,last,size)=>{
    //return new Promise((res,rej)=>{
        return HealthReport.find({healthcase:healthcase_id}).exec()
        .then((arr)=>{
            var q={
                'healthreports.healthreport':{
                    $in: arr.map(m=>m._id)
                }
            };
            if(last)
                q._id={'$gt': last};
            var pagesize=Number(size)
            if(!pagesize)
                pagesize=10
            return HealthFeedback.find(q)
            .sort('-date')
            .limit(pagesize)
            .exec();
        });
    //})
}
exports.getDailyFeedback=(patient_id)=>{
    return DailyFeedback.find({patient:patient_id})
    .sort('-created')
    .exec();
}
exports.searchHealthcaseByAny=(user_id,q,filter)=>{
    var r=new RegExp(q,'i');
    if(filter=="doctor")
        return Doctor.find({user:{'$ne':user_id},name:{'$regex':r}})
                .populate('user')
                .exec();
    else if(filter=="symptom")
        return Symptom.find({user:{'$ne':user_id},name:{'$regex':r}})
                .populate('user')
                .exec();
    else if(filter=="illness")
        return Healthcase.find({user: {'$ne':user_id},illness:{'$regex':r}})
            .populate('user')
            .exec();
    else
        return Healthcase.find({user: {'$ne':user_id},'medicines.name':{'$regex':r}})
            .populate('user')
            .exec();
}
exports.getDailyEmailsForPatients=()=>{
    return new Promise((res,rej)=>{
        Patient.find({}).populate('user').exec().then((list)=>{
            var emails={};
            list.forEach((p)=>{
                if(emails[p.user.email])
                    emails[p.user.email].push({name:p.name,id:p._id});
                else
                    emails[p.user.email]=[{name:p.name,id:p._id}];
            })
            res(emails);
        })
        .catch((e)=>{rej(e);});
    });
};
exports.getActiveHealthReportsForAll=()=>{
    return new Promise((res,rej)=>{
        HealthReport.find({enabled: true})
        .populate('patient')
        .populate('user')
        .exec()
        .then((hr_arr_full)=>{
            var emails={};
            hr_arr_full.forEach((hr)=>{
                var p=hr.patient;
                if(hr.user.settings){
                    var en=hr.user.settings.email_notification || [];
                    if(en.length){
                        for(var j=0;j<en.length;j++){
                            if(en[j].patient==p._id){
                                if(en[j].send_individual){
                                    var email=en[j].email?en[j].email:p.email;
                                    if(email){
                                        if(emails[email]==undefined)
                                            emails[email]=[];
                                        emails[email].push({symptom:hr.symptom,healthreport:hr._id});
                                    }
                                }else{
                                    if(emails[hr.user.email]==undefined)
                                        emails[hr.user.email]=[];
                                    emails[hr.user.email].push({symptom:hr.symptom,healthreport:hr._id});
                                }
                                break;
                            }
                        }
                    }else{
                        var email=p.email;
                        if(email){
                            if(emails[email]==undefined)
                                emails[email]=[];
                            emails[email].push({symptom:hr.symptom,healthreport:hr._id});
                        }
                    }
                }
            })
            res(emails);
        })
        .catch((e)=>{
            rej(e)
        });
    });
}
exports.getDashboardData=(user_id,pat_id)=>{
    return new Promise((res,rej)=>{
        var data={
            patients:0,
            doctors:0,
            healthcases:0,
            medicines:0,
            last5Illnesses:[],
            last5Symptoms:[]
        }
        var promises=[];
        promises.push(Patient.count({user:user_id}).exec().then(function(c){
            data.patients=c;
        }));
        promises.push(Doctor.count({user:user_id}).exec().then(function(c){
            data.doctors=c;
        }))
        promises.push(Healthcase.count({user:user_id}).exec().then(function(c){
            data.healthcases=c;
        }))
        promises.push(Medicine.count({user:user_id}).exec().then(function(c){
            data.medicines=c;
        }))
        promises.push(Healthcase.find({patient:pat_id}).sort('-created').limit(5).exec().then(function(arr){
            data.last5Illnesses=arr.map((h)=>{ return {illness:h.illness,date:h.startDate}});
        }))
        promises.push(Symptom.find({user:user_id}).sort('-created').limit(5).exec().then(function(arr){
            data.last5Symptoms=arr.map(s=>s.name);
        }))
        Promise.all(promises).then(()=>{
            res(data);
        })
        .catch((e)=>{
            rej(e);
        })
    })
}
exports.deleteAllPatientsNotMe=(user_id)=>{
    return Patient.remove({user:user_id,relation:{"$ne":"me"}}).exec();
}