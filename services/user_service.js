var User = require('../models').user;
var fs=require('fs')
var healthcase_service=require('./healthcase_service');
var Notification=require('../models').notification;

exports.userCreate=(user)=>{
    /*return new Promise((res,rej)=>{
        User.findOne({ username: user.username })
            .select('enabled')
            .exec((err, doc)=>{
                if(err) rej(err);
                else{
                    if(doc) rej({
                                    message: "username exists",
                                    name: "ValidationError"
                                });
                    else{
                        User.create(user,(err2,small)=>{
                            if(err2) rej(err2);
                            else res(small);
                        });
                    }
                }
            });
    })*/
    return User.create(user);
}
exports.getUserByUsernameAndToken=(username,type,token)=>{
    return User.findOne({username: username,'auth_token.token_type':type,'auth_token.token':token}).exec();
}
var getUser=exports.getUser=(id)=>{
    return User.findById(id)
            .select(User.DTOPropsFull)
            .exec();
}
exports.getUserByEmail=(email)=>{
    return User.findOne({email})
            .select(User.DTOPropsAuth)
            .exec();
}
exports.getUserByCredentials=(username,password)=>{
    return User.findOne({username,password})
            .select(User.DTOPropsFull)
            .exec();
}
exports.userList=(size,last)=>{
    size=(typeof size == undefined) ? 10 : Number(size);
    var query={};
    if(last) query['_id']={ $gt: last};
    return User.find(query)
        .select(User.DTOProps)
        .limit(size)
        .exec();
}
exports.getNonAdminUsers=()=>{
    return User.find({roles:{"$nin":["admin"]}}).exec();
}
exports.updateUser=(id,data)=>{
    return User.findByIdAndUpdate(id,data)
            .exec();
}
exports.count=()=>{
    return User.count().exec();
}
exports.changePassword=(id,oldp,newp)=>{
    return new Promise((res,rej)=>{
        User.findOne({_id:id,password:oldp})
        .then((user)=>{
            user.password=newp
            user.save()
            .then((u)=>{
                res({status:1,msg:"Password has been changed."})
            })
            .catch((err)=>{
                rej({status:0,msg:"Password update failed.",error:err});
            })
        })
        .catch((err)=>{
            rej({status:2,msg: 'Invalid Password.',error:err});
        })
    })
}
exports.saveProfilePhoto=(user_id,file,filename)=>{
    return getUser(user_id)
    .then((user)=>{
        var split
        if(filename){
            split=filename.split(".");
        }else{
            split=user.info.photo.split('.')
        }
        var name='pp_'+user_id+'_'+file.filename+'.'+split[split.length-1];
        if(user.info.photo){
            fs.unlink('upload/'+user.info.photo,(e)=>{
                console.log('file delete error',e);     
            });
        }
        fs.rename('tmp/'+file.filename,'upload/'+name);
        user.info.photo=name;
        return user.save();
    })
}
exports.reLogin=(req,user_id)=>{
    return new Promise((res,rej)=>{
        getUser(user_id)
        .then((user)=>{
            var oldselection=req.session.patient._id;
            req.login(user, (err)=>{
                if (err){
                    rej(err)
                    return err;
                }
                req.session.tempFiles=[];
                healthcase_service.listPatient(user_id)
                .then((pats)=>{
                    req.session.patients=pats;
                    for(var i=0;i<pats.length;i++)
                        if(pats[i]._id==oldselection){
                            req.session.patient=pats[i];
                            break;
                        }
                    req.session.fetch_patient=null;
                    res(user);
                })
                .catch((er)=>{
                    rej(er);
                })
            });
        })
        .catch((e)=>{
            rej(e);
        })
    })
}
exports.createNotification=(user_id,type,text,url)=>{
    return Notification.create({
            notification_type:type,
            text:text,
            url:url,
            user:user_id
        });
}
exports.getFewNotifications=(user_id)=>{
    return Notification.find({user:user_id,read:false}).limit(5).sort('-_id').exec();
}
exports.getNotifications=(user_id)=>{
    return Notification.find({user:user_id}).sort('-_id').exec();
}
exports.changeNotificationState=(id)=>{
    return Notification.findOneAndUpdate({_id:id},{read:true}).exec();
}
exports.getDemoUser=()=>{
    return User.findOne({demo_user:true}).exec();
}