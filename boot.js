var app = require('./index');
//setup
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var passport = require('passport');
var passportLocal = require('passport-local');
var helmet = require('helmet');
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var fs=require('fs');

var csrf = require('csurf');
var csrfProtection=exports.csrfProtection = csrf({ cookie: true });

app.use(cookieParser());
app.use(expressSession({
    secret:process.env.SESSION_SECRET || "verysecret",
    resave: true,
    saveUninitialized: true 
}));

app.use(require('connect-flash')());
//end setup
app.use(passport.initialize());
app.use(passport.session());

const view=require('./utils').view;
//custom middleware
app.use(function(req,res,next){//provide access to request object from response
    res.locals.request=req;
    res.locals.view=view;
    next();
})

const config = require('./config');

var mongoose = require('mongoose');
mongoose.Promise=global.Promise;
mongoose.plugin((schema, options) => {
    var indexes = schema.indexes();
    if (indexes.length == 0) return;
    var postHook = (error, _, next) => {
        //console.log('schema',indexes);
        if (error.name == 'MongoError' && error.code == 11000) {
            var regex = /index: (.+) dup key:/;
            var matches = regex.exec(error.message);
            if (matches) {
                matches = matches[1];
                for (var i = 0; i < indexes.length; i++) {
                    for (var field in indexes[i][0])
                        if (indexes[i][1].unique && (matches.indexOf('$' + field) > 0 || matches.startsWith(field+'_')) ){
                            var e = {}
                            e[field] = new mongoose.Error.ValidatorError({
                                type: 'unique',
                                path: field,
                                message: '"' + field + '" already exist'
                            })
                            var beautifiedError = new mongoose.Error.ValidationError();
                            beautifiedError.errors = e;
                            return next(beautifiedError);
                        }
                }
            }
        }
        next(error);
    }
    schema.post('save', postHook);
    schema.post('update', postHook);
    schema.post('findOneAndUpdate', postHook);
})
mongoose.connect(config.mongoURI,{ useMongoClient: true}).then(()=>{
    require('./seeders').seed();
})

var user_service = require('./services').user_service;

passport.use(new passportLocal.Strategy((username,password,doneCallback)=>{
    //access db and fetch user by username and password
    /*
    doneCallback(null,user)//success
    doneCallback(null,null)//bad or username missing
    doneCallback(new Error("Internal Error!"))//internal error
    */
    user_service.getUserByCredentials(username,password)
    .then((user)=>{
        if(!user)
            doneCallback(null,false,{message:'Wrong credential'});
        else if(user.account_expired)
            doneCallback(null,false,{message:'Account has expired'});
        else if(user.credential_expired)
            doneCallback(null,false,{message:'Your credential has expired'});
        else if(!user.enabled)
            doneCallback(null,false,{message:'Account is not activated'});
        else if(user.account_locked)
            doneCallback(null,false,{message:'Account is locked'});
        else
            doneCallback(null,user);
    })
    .catch((err)=>{
        doneCallback(err);
    });
}));
passport.serializeUser((user,doneCallback)=>{
    doneCallback(null, user._id);
});
passport.deserializeUser((id, doneCallback)=>{
    user_service.getUser(id)
    .then((user)=>{
        doneCallback(null,user);
    })
    .catch((err)=>{
        doneCallback(new Error("Internal Error!"));
    });
});

exports.authenticateLogin=(req,res,next,cb)=>{
    passport.authenticate('local',cb)(req,res,next);
};

var mailTransporter = require('node-mailjet').connect(config.email.api_key, config.email.api_secret);
const noMail=false;//no mail for quick testing

/*var sendEmail=exports.sendEmail=(to,subject,html,from=config.email.auth.user)=>{
    if(noMail) return html;
    return mailTransporter.sendMail({
        from: from,
        to: to, 
        subject: subject,
        html: html
    })
}*/
var sendEmail = exports.sendEmail = (to, subject, html,from_name=config.name,attach=null, from = config.email.from) => {
    if (noMail) return console.log(html);
    var mailRequest={
        FromEmail: from,
        FromName: from_name,
        Subject: subject,
        'Html-part': html,
        Recipients: [{'Email': to}]
    };
    if(attach)
        mailRequest['Attachments']=attach;
    return mailTransporter.post("send")
        .request(mailRequest);
}
exports.sendEmailConfirm=(to,url)=>{
    return new Promise((resolve,reject)=>{
        require('nunjucks').render('email/confirm.html',{url:url},(err,str)=>{
            if(err) reject(err);
            else resolve(sendEmail(to,'Account Verification',str))
        })
    })
}
exports.sendEmailForgot=(to,url)=>{
    return new Promise((resolve,reject)=>{
        require('nunjucks').render('email/forgot.html',{url:url},(err,str)=>{
            if(err) reject(err);
            else resolve(sendEmail(to,'Reset Password',str))
        })
    })
}
exports.sendTextEmailWithAttachment=(to,subject,body,attach)=>{
    return new Promise((res,rej)=>{
        fs.readFile(attach[0].path,(err,data)=>{
            if(err) return rej(err);
            res(sendEmail(to,subject,body,config.name,[{
                Filename:attach[0].filename,
                "Content-type":"application/pdf",
                content: data.toString('base64')
            }]))
        });
    })
}