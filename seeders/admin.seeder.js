var user_service=require('../services').user_service;
var healthcase_service=require('../services').healthcase_service;

module.exports=()=>{
    user_service.count().then((count)=>{
        if(count==0){
             user_service.userCreate({
                name:"ADMINISTRATOR",
                email:"admin@mail.com",
                username:"admin",
                password:"1234",
                enabled:true,
                roles:['admin']
            }).then((doc)=>{
                return healthcase_service.createSymptoms([
                    {
                        name:"Headache",
                        user: doc._id
                    },{
                        name:"Toothache",
                        user: doc._id
                    },{
                        name:"Leg pain",
                        user: doc._id
                    },{
                        name:"Sore Throat",
                        user: doc._id
                    }
                ])
            })
        }
    }).catch((e)=>{
        console.log("No seeding needed.");
    })
}