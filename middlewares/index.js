var createError=require('http-errors');
var multer  = require('multer');
var healthcase_service=require('../services').healthcase_service;
var user_service=require('../services').user_service;

exports.shouldLogin=(req,res,next)=>{
    if(req.isAuthenticated()){
        res.locals.user = req.user;
        next();
    }else res.redirect('/login?next='+req.originalUrl);// res.status(403).end();
};
var getPatientsAndMe=(user_id,oldselection)=>{
    return new Promise((res,rej)=>{
        healthcase_service.listPatient(user_id)
        .then((pats)=>{
            var data={
                patients:pats,
                patient:0
            };
            for(var i=0;i<pats.length;i++)
                if(pats[i]._id==oldselection){
                    data.patient=i;
                    break;
                }
            res(data);
        })
        .catch((err)=>{
            rej(err);
        })
    })
}
exports.userSession=(req,res,next)=>{
    if(!req.session.fetch_notification && !req.session.fetch_patient)
        next();
    else{
        var promises=[];
        if(req.session.fetch_patient)
            promises.push(getPatientsAndMe(req.user._id,req.session.patient._id));
        if(req.session.fetch_notification)
            promises.push(user_service.getFewNotifications(req.user._id))
        Promise.all(promises)
        .then((data)=>{
            if(data[0].patients){
                req.session.patients=data[0].patients;
                req.session.patient=data[0].patients[data[0].patient];
                req.session.fetch_patient=null;
                data.shift();
            }
            if(data.length>0){
                req.session.notifications=data[0];
                req.session.fetch_notification=null;
            }
            next();
        })
        .catch((e)=>{
            next();
        })
    }
}
exports.hasRole=(roles)=>{
    return (req,res,next)=>{
        if(!req.isAuthenticated())
            res.redirect('/login?next='+req.originalUrl);//res.status(403).end();// next(new Error("You are not allowed to access this page"));
        else{
            var has=false;
            if(Array.isArray(roles)){
                roles.forEach(function(role){
                    if(Array.isArray(req.user.roles))
                        if(req.user.roles.indexOf(role)>=0)
                            has=true;
                });
            }else if(typeof roles == "string"){
                if(Array.isArray(req.user.roles))
                    if(req.user.roles.indexOf(roles)>=0)
                        has=true;
            }
            if(!has)
                return next(createError(403, 'You are not allowed to access this page', { code: 'ENEEDROLE'}));
            next();
        }
    };
};
exports.tempUpload=multer({ dest: 'tmp/' });
exports.persistUpload=multer({dest: 'upload/'});