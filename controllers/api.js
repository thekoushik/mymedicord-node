var securityManager=require('../index').securityManager;
var services=require('../services');
var util=require('../utils');
var config = require('../config');
var fs=require('fs');

exports.setUserPosition=(req,res)=>{
  services.user_service.updateUser(req.user._id,{
    info:{
      lat:req.body.lat,
      lng:req.body.lng
    }
  })
  .then((u)=>{
    return services.user_service.reLogin(req,u._id)
  })
  .then((user)=>{
    res.json({status:1,msg:"Location updated."});
  })
  .catch((er)=>{
    res.status(500).send(er);
  })
}

exports.searchIllness=(req,res)=>{
  services.healthcase_service.findIllness(req.query.q,req.user)
  .then((docs)=>{
    res.json(docs.map((d)=>{ return {id:d.illness,text:d.illness} }))
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
};
exports.searchSymptoms=(req,res)=>{
  services.healthcase_service.findSymptoms(req.query.q,req.user)
    .then((docs)=>{
      res.json(docs.map((d)=>{return {id:d._id,text:d.name}}));
    })
    .catch((err)=>{
      res.status(500).send(err);
    });
};
exports.userList=function(req, res) {
  services.user_service.userList(req.query.size,req.query.last)
    .then((list)=>{
      res.json(list)
    })
    .catch((err)=>{
      res.status(500).send(err);
    })
};
exports.user=function(req,res){
  services.user_service.getUser(req.params.id)
    .then((user)=>{
      res.status(200).json(user)
    })
    .catch((err)=>{
      res.status(500).send(err)
    })
};
exports.userCreate=function(req,res){
  var userdata=req.body;
  var verifyToken=util.createToken();
  userdata.auth_token=verifyToken;
  const token=util.encodeAuthToken(userdata.username,verifyToken.token);
  services.user_service.userCreate(userdata)
    .then((user)=>{
      services.healthcase_service.createPatient({
        user:user._id,
        name: user.name,
        email: user.email,
        relation: "me"
      })
      services.user_service.createNotification(user._id,'register','You joined.','');
      securityManager
        .sendEmailConfirm(user.email,config.url+'/verify?token='+token)
        .then((response)=>{})
        .catch((err)=>{});
      res.status(201).location(req.baseUrl+req.path+"/"+user._id).end();
    })
    .catch((err)=>{
      if(typeof err == 'object'){
        if(err.code==11000){
          res.status(422).json({message:'Uniqueness violation'});
        }else
          res.status(422).json(err);
      }else
        res.status(500).send(err);
    })
};
exports.info=(req, res)=>{
  services.user_service.getUser(req.user._id)
  .then((user)=>{
    res.json(user);
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
};
exports.doctorList=(req,res)=>{
  services.healthcase_service.listDoctors(req.user)
    .then((list)=>{
      res.json(list)
    })
    .catch((err)=>{
      res.status(500).send(err);
    })
}
exports.patientList=(req,res)=>{
  services.healthcase_service.listPatient(req.user)
    .then((list)=>{
      res.json(list);
    })
    .catch((err)=>{
      res.status(500).send(err);
    })
}
exports.createHealthcase=(req,res)=>{
    var hcdata=req.body;
    var user =req.user._id;
    hcdata.user=user;
    var newsymp=[];
    var oldsymp=[];
    hcdata.symptoms.forEach(function(s){
        if(s.startsWith("__"))
            newsymp.push({user: user,name:s.substr(2)})
        else
            oldsymp.push(s)
    })
    if(hcdata.medicines)
      hcdata.medicines=hcdata.medicines.map(m=>{
        m.user=user
        return m
      });
    const patient_id=hcdata.patient;
    if(hcdata.files)
      hcdata.files=hcdata.files.map((f)=>{
          var split=f.file.originalname.split('.');
          var name=patient_id+'_'+f.file.filename+'.'+split[split.length-1];
          fs.rename('tmp/'+f.file.filename,'upload/'+name);
          return {
            date:f.date,
            report_type: f.report_type,
            file:name
          };
      })
    
    if(newsymp.length){
      services.healthcase_service.createSymptoms(newsymp)
        .then((symps)=>{
            hcdata.symptoms=oldsymp.concat(symps.map(s=>s._id))
            return services.healthcase_service.createHealthCase(hcdata)
        })
        .then((hc)=>{
            res.json({status: 1, msg: "Healthcase created",id:hc._id});
        })
        .catch((err)=>{
            res.status(500).send(err);
        })
    }else{
      services.healthcase_service.createHealthCase(hcdata)
        .then((hc)=>{
            res.json({status: 1, msg: "Healthcase created",id:hc._id});
        })
        .catch((err)=>{
            res.status(500).send(err);
        })
    }
}
exports.viewHealthcase=(req,res)=>{
  services.healthcase_service.viewHealthcase(req.params.id)
  .then((hc)=>{
    res.json(hc);
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
}
function getProcessedSymptoms(symptoms,user){
  return new Promise(function(res,rej){
    var newsymp=[];
    var oldsymp=[];
    symptoms.forEach(function(s){
        if(s.startsWith("__"))
            newsymp.push({user: user,name:s.substr(2)})
        else
            oldsymp.push(s)
    })
    if(newsymp.length){
      return services.healthcase_service.createSymptoms(newsymp)
        .then((symps)=>{
            res(oldsymp.concat(symps.map(s=>s._id)));
        })
        .catch((err)=>{
          rej(err);
        })
    }else{
      res(oldsymp);
    }
  });
}
exports.edit_healthcase_post=(req,res)=>{
  var user=req.user._id;
  services.healthcase_service.findHealthcase(req.params.id,req.user._id)
  .then((hc)=>{
    var healthcase_data=req.body;
    if(healthcase_data.medicines)
      healthcase_data.medicines=healthcase_data.medicines.map(m=>{
        if(!m.user)
          m.user=req.user._id
        return m
      });
    const ds=healthcase_data.deleted_symptoms;
    //if(ds!=undefined) delete healthcase_data.deleted_symptoms;
    services.healthcase_service.removeHealthReportsAndFeedbacksBySymptoms(ds)
    .then((_)=>{
      return getProcessedSymptoms(healthcase_data.symptoms,user); 
    })
    .then((symps)=>{
      healthcase_data.symptoms=symps;
      return services.healthcase_service.saveHealtcase(hc,healthcase_data)
    })
  })
  .then((hc)=>{
    res.json({status:1,msg:"Healthcase saved."})
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
}
exports.new_patient_post=(req,res)=>{
  var data=req.body;
  data.user=req.user._id
  services.healthcase_service.createPatient(data)
  .then((pat)=>{
    req.session.fetch_patient=true;
    res.json({status:1,msg:"Patient created.",id:pat._id});
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
}
exports.get_patient=(req,res)=>{
  services.healthcase_service.getPatient(req.params.id)
  .then((p)=>{
    res.json(p);
  })
  .catch((e)=>{
    res.status(500).send(e);
  })
}
exports.edit_patient_post=(req,res)=>{
  var user_id=req.user._id;
  var patient_data=JSON.parse(req.body.patient);
  patient_data.user=user_id;
  //var old_photo=null;
  //var new_photo=null;
  services.healthcase_service.getPatient(req.params.id)
  .then((patient)=>{
    ["name","email","relation","blood","info"].forEach(function(d){
        if(patient_data[d])
          patient[d]=patient_data[d];
    })
    if(req.file){
        var file=req.file;
        var split=req.body.filename.split(".");
        var name='pp_'+user_id+'_pat_'+file.filename+'.'+split[split.length-1];
        if(patient.info.photo){
          fs.unlink('upload/'+patient.info.photo,(e)=>{
              console.log('file delete error',e);     
          });
        }
        patient.info.photo=name;
        fs.rename('tmp/'+file.filename,'upload/'+name);
    }
    return patient.save();
  })
  .then((p)=>{
    res.json({status:1,msg:"Patient updated"});
  })
  .catch((e)=>{
    res.status(500).send(e);
  })
}
exports.new_doctor_post=(req,res)=>{
  var data=req.body;
  data.user=req.user._id;
  services.healthcase_service.createDoctor(data)
  .then((doc)=>{
    res.json({status:1,msg:"Doctor created.",id:doc._id})
  })
  .catch((err)=>{
    res.status(500).send(err)
  })
}
exports.save_evolution=(req,res)=>{
  services.healthcase_service.saveEvolutionByDate(req.body,req.session.patient._id)
  .then((pat)=>{
    res.json({msg:"Saved"});
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
}
exports.growth_list=(req,res)=>{
  services.healthcase_service.getEvolutionGrowth(req.session.patient._id)
  .then((data)=>{
    res.json(data);
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
}
exports.getHealthcasesByPatient=(req,res)=>{
  services.healthcase_service.listHealthcaseByPatient(req.session.patient._id)
  .then((list)=>{
    res.json(list);
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
}
exports.saveProfileBasic=(req,res)=>{
  services.user_service.updateUser(req.user._id,req.body)
  .then(user=>services.user_service.getUser(user._id))
  .then((user)=>{
    services.user_service.createNotification(user._id,'settings_change','Basic Profile Updated');
    req.session.fetch_notification=true;
    res.json(user);
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
}
exports.saveProfilePrivacy=(req,res)=>{
  services.user_service.updateUser(req.user._id,req.body)
  .then(user=>services.user_service.getUser(user._id))
  .then((user)=>{
    res.json(user);
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
}
exports.saveProfilePassword=(req,res)=>{
  services.user_service.changePassword(req.user._id,req.body.password,req.body.newpassword)
  .then((j)=>{
    res.json(j)
  })
  .catch((j)=>{
    res.json(j)
  })
}
exports.saveProfilePhoto=(req,res)=>{
  services.user_service.saveProfilePhoto(req.user._id,req.file,req.body.filename)
  .then((user)=>{
    res.json({status:1,name:user.info.photo});
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
}
exports.getHealthReport=(req,res)=>{
  services.healthcase_service.getHealthFeedbacksByHealthcase(req.params.healthcase_id,req.query.last,req.query.size)
  .then((arr)=>{
    res.json(arr);
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
}
exports.getHealthReportByActivePatient=(req,res)=>{
  services.healthcase_service.getHealthReportByPatient(req.session.patient._id)
  .then((arr)=>{
    res.json(arr);
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
}
exports.searchHealtcaseByAny=(req,res)=>{
  services.healthcase_service.searchHealthcaseByAny(req.user._id,req.body.q,req.body.filter)
  .then((hcs)=>{
    res.json(hcs);
  })
  .catch((err)=>{
    res.status(500).send(err);
  })
}
exports.getDashboard=(req,res)=>{
  services.healthcase_service.getDashboardData(req.user._id,req.session.patient._id).then((data)=>{
    res.json(data);
  })
  .catch((e)=>{
    res.status(500).send(e);
  })
}
exports.readNotification=(req,res)=>{
  services.user_service.changeNotificationState(req.params.id)
  .then((data)=>{
    for(var i=0;i<req.session.notifications.length;i++){
      if(req.session.notifications[i]._id==req.params.id){
        req.session.notifications.splice(i,1);
        break;
      }
    }
    res.status(200).end();
  })
  .catch((e)=>{
    res.status(500).send(e);
  })
}