var user_service =require('../services').user_service;

module.exports.dashboard=(req,res,next)=>{
    user_service.getNonAdminUsers()
    .then((users)=>{
        res.render('admin/dashboard.html',{users:users});
    })
    .catch((e)=>{
        next(e);
    })
}