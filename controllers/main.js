var user_service = require('../services').user_service;
var healthcase_service=require('../services').healthcase_service;
var fs=require('fs');
var utils = require('../utils');
var pdf=require('html-pdf');
var nunjucks=require('nunjucks');
const PDFMerge = require('pdf-merge');
var securityManager = require('../index').securityManager;

exports.index=(req,res)=>{
    if(req.isAuthenticated()){
        res.redirect(req.user.roles.indexOf("admin")>=0?'/admin':'/dashboard');
    }else
        res.render('index.html');
};
exports.notFound=(req,res)=>{
    res.render('error/404.html',{origin:req.originalUrl});
};
exports.errorHandler=(err, req, res, next)=>{
  if (err.code === 'EBADCSRFTOKEN') res.status(403).send('Hack Attempt!');
  else if(err.code === 'ENEEDROLE') res.render("error/403.html");
  else return next(err);
};
exports.dashboard=(req,res)=>{
    //req.flash('success','Thank you for registering.');
    res.render('user/dashboard.html')
}
exports.profile=(req,res)=>{
    res.render('user/profile.html');
}
exports.save_profile=(req,res,next)=>{
    user_service.updateUser(req.user._id,{name: req.body.name})
        .then((user)=>{
            req.login(user, (err)=>{
                if (err) return next(err);
                return res.redirect('/profile');
            });
        })
}
exports.create_healthcase_page=(req,res)=>{
    res.render('user/create_healthcase.html');
}
exports.list_doctor_page=(req,res)=>{
    healthcase_service.listDoctors(req.user._id)
        .then((docs)=>{
            res.render('user/doctor_list.html',{doctors:docs});
        })
        .catch((err)=>{
            res.render('error/500.html');
        })
}
exports.new_doctor_page=(req,res)=>{
    res.render('user/new_doctor.html');
}
exports.new_doctor_post=(req,res)=>{
    var doc=req.body;
    doc.user=req.user._id;
    healthcase_service.createDoctor(doc)
    .then((docs)=>{
        res.json({status:1,msg:"Doctor added"});
    })
    .catch((err)=>{
        res.json({status:0,msg:utils.mongooseErrorToString(err),err:err});
    });
}
exports.new_patient_page=(req,res)=>{
    res.render('user/new_patient.html');
}
exports.new_patient_post=(req,res)=>{
    var user_id=req.user._id;
    var patient=JSON.parse(req.body.patient);
    if(req.file){
        var file=req.file;
        var split=req.body.filename.split(".");
        var name='pp_'+user_id+'_pat_'+file.filename+'.'+split[split.length-1];
        patient.info.photo=name;
        fs.rename('tmp/'+file.filename,'upload/'+name);
    }
    patient.user=user_id;
    //if(patient.info.dob) patient.info.dob=new Date(patient.info.dob);
    healthcase_service.createPatient(patient)
    .then((docs)=>{
        req.session.fetch_patient=true;
        req.session.save((err)=>{
            console.log('session save err',err);
        });
        //req.session.reload();
        //res.header('Access-Control-Allow-Credentials', 'true');
        res.json({status:1,msg:"Patient added"});
    })
    .catch((err)=>{
        res.json({status:0,msg:"Error occured",err:err});
    });
}
exports.list_patient_page=(req,res)=>{
    healthcase_service.listPatient(req.user._id)
        .then((docs)=>{
            res.render('user/patient_list.html',{patients:docs});
        })
        .catch((err)=>{
            res.render('error/500.html');
        })
}
exports.list_healthcase_page=(req,res)=>{
    healthcase_service.listHealthcaseSortByDate(req.user,req.session.patient)
    .then((hcs)=>{
        res.render('user/healthcase_list.html',{list: hcs});
    })
    .catch((err)=>{
        res.render('error/500.html');
    })
}
exports.view_healthcase=(req,res)=>{
    healthcase_service.viewHealthcase(req.params.id)
    .then((hc)=>{
        res.render('user/healthcase_view.html',{healthcase: hc})
    })
    .catch((err)=>{
        res.render('error/500.html')
    })
}
exports.edit_patient_page=(req,res)=>{
    /*healthcase_service.findPatient(req.params.id,req.user._id)
    .then((patient)=>{
        res.render('user/edit_patient.html',{patient:patient});
    })
    .catch((err)=>{
        res.render('error/500.html');
    })*/
    res.render('user/edit_patient.html',{patient_id:req.params.id});
}
/*exports.edit_patient_post=(req,res)=>{
    healthcase_service.findPatient(req.params.id,req.user._id)
    .then((patient)=>{
        var body=req.body
        body.info={};
        console.log(body.dob);
        ['phone','dob','gender'].forEach((f)=>{
            if(body[f]){
                body.info[f]=body[f]
                //delete body[f]
            }
        })
        if(body.info.dob)
            body.info.dob=moment(body.info.dob,'dddd DD MMMM YYYY').startOf('day').format('YYYY-MM-DD');
        return healthcase_service.savePatient(patient,body)
    })
    .then((patient)=>{
        req.flash('success','Patient updated.');
        res.redirect('/patients/all');
    })
    .catch((err)=>{
        console.log(err);
        res.render('error/500.html');
    })
}*/
exports.edit_doctor_page=(req,res)=>{
    healthcase_service.findDoctor(req.params.id,req.user._id)
    .then((doctor)=>{
        res.render('user/edit_doctor.html',{doctor:doctor});
    })
    .catch((err)=>{
        res.render('error/500.html');
    })
}
exports.edit_doctor_post=(req,res)=>{
    healthcase_service.findDoctor(req.params.id,req.user._id)
    .then((doctor)=>{
        return healthcase_service.saveDoctor(doctor,req.body)
    })
    .then((doctor)=>{
        req.flash('success','Doctor updated.');
        res.redirect('/doctors/all');
    })
    .catch((err)=>{
        res.render('error/500.html');
    })
}
exports.edit_healthcase_page=(req,res)=>{
    res.render('user/edit_healthcase.html',{healthcase_id:req.params.id});
}
exports.track_healthcase_page=(req,res)=>{
    healthcase_service.listHealthcaseSortByDate(req.user,req.session.patient)
    .then((hc)=>{
        res.render("user/track_healthcase.html",{list:hc,healthcase_id:req.query.id});
    })
    .catch((err)=>{
        res.render('error/500.html');
    })
}
exports.upload_healthcase_page=(req,res)=>{
    healthcase_service.listHealthcaseSortByDate(req.user,req.session.patient)
    .then((hc)=>{
        res.render('user/upload_healthcase.html',{healthcases:hc});
    })
}
exports.upload_healthcase_post=(req,res)=>{
    healthcase_service.readExcel(req.file.path)
    .then((json)=>{
        //console.log(json);
        return healthcase_service.jsonToHealthcase(json,req.user,req.session.patient);
    })
    .then((arr)=>{
        var count=arr.length;
        req.flash('success',count+' healthcase(s) uploaded.');
        res.redirect('/healthcases/upload');
    })
    .catch((err)=>{
        console.log(err);
        req.flash('error',utils.mongooseErrorToString(err));
        res.redirect('/healthcases/upload')
    })
}
exports.report_page=(req,res)=>{
    res.render('user/reports.html');
    /*healthcase_service.listHealthcaseByPatient(req.session.patient._id)
    .then((hcs)=>{
    })*/
}
exports.account_settings_page=(req,res)=>{
    res.render('user/account_settings.html');
    /*healthcase_service.findHealthcase(req.query.id,req.user)
    .then((data)=>{
    })
    .catch((err)=>{
        res.render('error/500.html');
    })*/
}
exports.evolution_page=(req,res)=>{
    res.render('user/evolution.html',{birth:req.session.patient.evolution.birth});
}
exports.evolution_birth_post=(req,res)=>{
    healthcase_service.saveEvolutionBirth(req.body,req.session.patient._id)
    .then((pat)=>{
        req.session.patient=pat;
        req.flash('success','Birth info saved.');
        res.redirect('/evolution');
    })
    .catch((err)=>{
        req.flash('error',err);
        res.redirect('/evolution');
    })
}
exports.upload_file=(req,res)=>{
    res.json(req.files[0]);
}
exports.remove_uploaded_temp_file=(req,res)=>{
    fs.unlink('tmp/'+req.body.name,_=>0);
    res.json({msg: "Request Taken"});
}
exports.growth_summery_page=(req,res)=>{
    healthcase_service.findPatient(req.session.patient._id,req.user._id)
    .then((pat)=>{
        res.render('user/growth_summery.html',{growth:pat.evolution.growth});
    })
    .catch((err)=>{
        res.render('error/500.html');
    })
}
exports.select_patient=(req,res)=>{
    healthcase_service.findPatient(req.params.id,req.user._id)
    .then((pat)=>{
        req.session.patient=pat;
        res.json({msg: 'Selected.'});
    })
    .catch((err)=>{
        console.log(err);
        res.status(500).send(err);
    })
}
exports.healthreport_public_page=(req,res)=>{
    healthcase_service.getPatient(req.params.patient_id)
    .then((pat)=>{
        healthcase_service.getActiveHealthReports(req.params.patient_id)
        .then((symps)=>{
            res.render('user/healthfeedback_public.html',{patient:pat,symptoms:symps})
        })
        .catch((err)=>{
            res.render('error/500.html');
        })
    })
}
exports.healthreport_public_post=(req,res)=>{
    healthcase_service.checkTodaysFeedback(req.params.patient_id)
    .then((a)=>{
        if(a==null)
            return healthcase_service.saveFeedback(req.body,req.params.patient_id);
        else if(Object.keys(req.body).length>2)
            return healthcase_service.updateEmptyFeedback(a._id,req.body,req.params.patient_id);
        else
            res.redirect('/feedback/thankyou?given');
    })
    .then((arr)=>{
        if(arr!=undefined){//because after previous 'then', this 'then' callback will allways run even if previous callback does not return a promise
            res.redirect('/feedback/thankyou');
        }
    })
    .catch((e)=>{
        console.log('feedback error',e);
        res.render('error/500.html');
    })
}
exports.thankyou=(req,res)=>{
    res.render('user/thankyou.html');
}
exports.search_map=(req,res)=>{
    res.render('user/map.html',{q:req.query});
}
var healthcaseToPDF=(id)=>{
    return new Promise((res,rej)=>{
        healthcase_service.viewHealthcase(id)
        .then((hc)=>{
            nunjucks.render('email/healthcase_share.html',{healthcase:hc},(err,str)=>{
                if(err) rej(err);
                else{
                    if(hc.files.length==0){
                        pdf.create(str, { format: 'Letter' }).toStream((err2, stream)=>{
                            if (err2){
                                rej(err2); return;
                            }
                            res(stream);
                        });
                    }else{
                        pdf.create(str, { format: 'Letter' }).toFile('./tmp/'+utils.newTempFileName()+'.pdf',(err2, obj)=>{
                            if (err2){
                                rej(err2); return;
                            }
                            var files=[obj.filename];
                            files=files.concat(hc.files.map((m)=>'./upload/'+m.file));
                            PDFMerge(files, {output: 'Stream'}).then((stream) => {
                                fs.unlink(obj.filename,(err3)=>{
                                    res(stream);
                                });
                            })
                            .catch((e)=>{
                                fs.unlink(obj.filename,(err3)=>{
                                    rej(e);
                                });
                            })
                        });
                    }
                }
            })
        })
        .catch((err)=>{
            rej(err);
        })
    })
}
exports.downloadHealthcase=(req,res)=>{
    healthcaseToPDF(req.params.id).then((stream)=>{
        res.attachment('Healthcase.pdf');
        stream.pipe(res);
    })
    .catch((e)=>{
        res.render('error/500.html',{err:e});
    })
}
exports.shareHealthcaseEmail=(req,res)=>{
    var to=req.body.to;
    var subject=req.body.subject;
    var body=req.body.content;
    healthcaseToPDF(req.body.id).then((stream)=>{
        const temp='./tmp/'+utils.newTempFileName()+'.pdf';
        var writer=fs.createWriteStream(temp);
        writer.on('finish', function () {
            securityManager.sendTextEmailWithAttachment(to,subject,body,[{
                filename:'Healthcase.pdf',
                path: temp
            }])
            .then((dd)=>{
                fs.unlink(temp,(ee)=>{
                    if(dd.response.statusCode==200)
                        res.json({status:200,message:'Email Sent'});
                    else
                        res.json({status:500,message:'Email Not Sent'});
                })
            })
            .catch((e)=>{
                res.status(500).send(e);
            })
        })
        stream.pipe(writer);
    })
    .catch((e)=>{
        res.status(500).send(e);
    })
}
exports.notification_list=(req,res)=>{
    user_service.getNotifications(req.user._id)
    .then((list)=>{
        res.render('user/notification_list.html',{list:list});
    })
    .catch((e)=>{
        res.render('error/500.html',{err:e});
    })
}